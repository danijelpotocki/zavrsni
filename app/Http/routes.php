<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::controller('account', 'App\Zavrsni\Account\Controller\AccountController');
Route::controller('estate', 'App\Zavrsni\Estate\Controller\EstateController');
Route::controller('map', 'App\Zavrsni\Map\Controller\MapController');
Route::controller('/', 'App\Zavrsni\Home\Controller\HomeController');
