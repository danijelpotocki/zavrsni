<?php
namespace App\Zavrsni\Location\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';

    public function user()
    {
        return $this->hasMany('App\Zavrsni\Account\Model\User', 'id', 'country_id');
    }
}