<?php

namespace App\Zavrsni\Location\Model;


use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'region';

    public function city()
    {
        return $this->hasMany('App\Zavrsni\Location\Model\City', 'region_id', 'id');
    }
}