<?php namespace App\Zavrsni\Location;

use Illuminate\Support\ServiceProvider;

/**
 * Class ServiceProvider
 * @package Voxterr\Account
 */
class LocationServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Register the service with the IoC container.
     *
     * @return void
     */
    public function register()
    {
        $ns = 'App\Zavrsni\Location';

        $this->app->bind('City', $ns . '\Model\City');
        $this->app->bind('Country', $ns . '\Model\Country');
        $this->app->bind('Region', $ns . '\Model\Region');
    }


    public function provides()
    {
        return [
            'City',
            'Country',
            'Region',
        ];
    }
}
