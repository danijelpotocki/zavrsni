<?php
namespace App\Zavrsni\Home\Controller;

use App\Http\Controllers\Controller;
use App\Zavrsni\Location\Model\City;
use App\Zavrsni\Location\Model\Region;

class HomeController extends Controller
{
    public function getIndex()
    {
        $cities = City::all();
        $regions = Region::all();

        return view('index.index', ['cities' => $cities, 'regions' => $regions]);
    }
}