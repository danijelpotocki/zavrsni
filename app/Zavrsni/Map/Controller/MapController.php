<?php
namespace App\Zavrsni\Map\Controller;

use App\Http\Controllers\Controller;
use App\Zavrsni\Estate\Model\Estate;
use App\Zavrsni\Location\Model\City;
use App\Zavrsni\Location\Model\Region;

class MapController extends Controller
{
    public function getIndex()
    {
        $cities = City::all();
        $regions = Region::all();
        $estates = Estate::all();

        return view('map.map', ['cities' => $cities, 'regions' => $regions, 'estates' => $estates]);
    }
}