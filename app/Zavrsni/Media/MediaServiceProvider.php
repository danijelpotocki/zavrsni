<?php namespace App\Zavrsni\Media;

use Illuminate\Support\ServiceProvider;

/**
 * Class ServiceProvider
 * @package Voxterr\Account
 */
class MediaServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Register the service with the IoC container.
     *
     * @return void
     */
    public function register()
    {
        $ns = 'App\Zavrsni\Media';

        $this->app->bind('Media', $ns . '\Model\Media');
    }


    public function provides()
    {
        return [
            'Media',
        ];
    }
}
