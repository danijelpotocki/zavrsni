<?php
namespace App\Zavrsni\Media\Model;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
}