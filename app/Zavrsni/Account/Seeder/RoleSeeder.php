<?php

namespace App\Zavrsni\Account\Seeder;


use DB;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{

    protected $table;

    public function __construct()
    {
        $this->table = DB::table('role');
    }

    public function run()
    {

        $this->table->delete();

        $data = [
            [ 'id' => 1, 'name' => 'administrator', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'id' => 2, 'name' => 'user', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
        ];

        $this->table->insert($data);
    }
}