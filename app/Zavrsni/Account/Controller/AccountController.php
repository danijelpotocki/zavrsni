<?php namespace App\Zavrsni\Account\Controller;

use App;
use App\Http\Controllers\Controller;
use App\Zavrsni\Account\Model\Role;
use App\Zavrsni\Account\Model\User;
use App\Zavrsni\Location\Model\Country;
use Auth;
use Input;
use Mail;

/**
 * AccountController Class
 *
 * Implements actions regarding user management.
 */
class AccountController extends Controller
{
    public function getIndex()
    {
        return view('account.index');
    }

    public function getLogin()
    {
        if (Auth::check()) {
            return \Redirect::to('/');
        }

        return view('account.login');
    }

    public function postLogin()
    {
        if (Auth::check()) {
            return \Redirect::to('/');
        }

        $input = [
            'email' => Input::get('email'),
            'password' => Input::get('password'),
        ];

        if (Auth::attempt($input)) {
            return \Redirect::to('/');
        } else {
            $error = 'Upisali ste pogrešni email ili lozinku.';
            return view('account.login', ['error' => $error]);
        }
    }

    public function getRegister()
    {
        if (Auth::check()) {
            return \Redirect::to('/');
        }

        $countries = Country::all();

        return view('account.register', ['countries' => $countries]);
    }

    public function postRegister()
    {
        if (Auth::check()) {
            return \Redirect::to('/');
        }

        $input = Input::all();

        $rules = [
            'email' => 'required|email|max:255|unique:user',
            'name' => 'required',
            'surname' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'password_confirmation' => 'sometimes|same:password',
            'country_id' => 'required',
        ];

        $validator = \Validator::make($input, $rules);
        if ($validator->fails()) {

            $countries = Country::all();
            $errors = $validator->errors();

            return view('account.register', ['countries' => $countries, 'errors' => $errors]);
        }

        $input['role_id'] = Role::where('code', \Constant::ROLE_USER)->first()->id;
        $input['is_active'] = 1;
        $input['password'] = bcrypt($input['password']);

        /* @var $user User */
        $user = App::make('User');
        $user->fill($input);
        $user->save();

        Auth::login($user);

        return \Redirect::to('/');
    }

    public function getForgotten()
    {
        return view('account.forgotten');
    }

    public function postForgotten()
    {
        $email = Input::get('email');

        $user = User::where('email', $email)->first();

        if (!$user) {
            $error = 'E-mail adresa koju ste upisali ne postoji.';
            return view('account.forgotten', ['error' => $error]);
        }

        $password = substr(str_shuffle(md5(microtime())), 0, 10);

        $user->password = bcrypt($password);
        $user->save();

        Mail::queue('email.forgotten', ['password' => $password],
            function ($message) use ($password, $email) {
                $message->to($email)->subject('Vaša nova lozinka');
            }
        );

        $success = 'Poslan vam je e-mail sa novom lozinkom. Ako nemate poruku u primljenim porukama, pogledajte pod spam!';
        return view('account.forgotten', ['success' => $success]);
    }

    public function getLogout()
    {
        Auth::logout();
        return \Redirect::to('/');
    }

    public function getProfil()
    {
        return view('account.profile');
    }

    public function getEdit()
    {
        $countries = Country::all();

        return view('account.edit_profile', ['countries' => $countries]);
    }

    public function postEdit()
    {
        $input = Input::all();

        $rules = [
            'email' => 'required|email|max:255|unique:user',
            'name' => 'required',
            'surname' => 'required',
            'phone' => 'required',
            'country_id' => 'required',
        ];

        $validator = \Validator::make($input, $rules);
        if ($validator->fails()) {

            $countries = Country::all();
            $errors = $validator->errors();

            return view('account.edit_profile', ['countries' => $countries, 'errors' => $errors]);
        }

        /* @var $user User */
        $user = Auth::user();
        $user->fill($input);
        $user->save();

        return view('account.profile');
    }

    public function postChangepassword()
    {
        $input = Input::all();

        $rules = [
            'old_password' => 'required',
            'password' => 'required',
            'password_confirmation' => 'sometimes|same:password',
        ];

        $user = Auth::user();
        $validator = \Validator::make($input, $rules);
        if ($validator->fails() || bcrypt($input['old_password']) != $user->password) {

            $countries = Country::all();
            $errors = $validator->errors();

            return view('account.edit_profile', ['countries' => $countries, 'password_errors' => $errors]);
        }

        $user->password = bcrypt($input['password']);
        $user->save();

        return view('account.profile');
    }
}
