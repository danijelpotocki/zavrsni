<?php namespace App\Zavrsni\Account\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'confirmation_code'];

    protected $fillable = ['email', 'password', 'title', 'name', 'surname', 'phone', 'country_id', 'is_active', ];

    protected $dates = [];
    
    public function role(){
        return $this->belongsTo('App\Zavrsni\Account\Model\Role', 'role_id', 'id');
    }

    public function country(){
        return $this->belongsTo('App\Zavrsni\Location\Model\Country', 'country_id', 'id');
    }

    public function estate(){
        return $this->hasMany('App\Zavrsni\Estate\Model\Estate', 'user_id', 'id');
    }
}
