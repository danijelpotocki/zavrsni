<?php

namespace App\Zavrsni\Account\Model;


use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';

    public function user()
    {
        return $this->hasMany('App\Zavrsni\Account\Model\User', 'id', 'role_id');
    }
}