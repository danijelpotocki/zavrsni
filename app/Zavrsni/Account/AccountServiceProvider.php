<?php namespace App\Zavrsni\Account;

use Illuminate\Support\ServiceProvider;

/**
 * Class ServiceProvider
 * @package Voxterr\Account
 */
class AccountServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Register the service with the IoC container.
     *
     * @return void
     */
    public function register()
    {
        $ns = 'App\Zavrsni\Account';

        $this->app->bind('User', $ns . '\Model\User');
        $this->app->bind('Role', $ns . '\Model\Role');

        $this->app->bind('UserRepository', $ns . '\Repository\UserRepository');

        $this->app->bind('RoleSeeder', $ns . '\Seeder\RoleSeeder');
        $this->app->bind('UserSeeder', $ns . '\Seeder\UserSeeder');
    }

    
    public function provides()
    {
        return [
            'User',
            'Role',
            'UserRepository',
            'RoleSeeder',
            'UserSeeder',
        ];
    }
}
