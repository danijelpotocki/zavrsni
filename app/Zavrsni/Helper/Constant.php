<?php

namespace App\Zavrsni\Helper;


class Constant
{
    const APARTMAN_TYPE_APARTMAN = 'Apartman';
    const APARTMAN_TYPE_SOBA = 'Soba';
    const APARTMAN_TYPE_STUDIO = 'Studio';
    const APARTMAN_TYPE_KUCA = 'Kuća';

    const ROLE_USER = 'user';
    const ROLE_ADMINISTRATOR = 'administrator';
}