<?php namespace App\Zavrsni\Helper;


use Illuminate\Support\ServiceProvider;

/**
 * Class ServiceProvider
 * @package Voxterr\Account
 */
class HelperServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Register the service with the IoC container.
     *
     * @return void
     */
    public function register()
    {
        $ns = 'App\Zavrsni\Helper';

        $this->app->bind('Helper', $ns . '\Helper');
        $this->app->bind('Constant', $ns . '\Constant');
    }


    public function provides()
    {
        return [
            'Helper',
            'Constant',
        ];
    }
}
