<?php

namespace App\Zavrsni\Helper;


use App\Zavrsni\Estate\Model\Apartman;

class Helper
{
    public static function brojac_apartman()
    {
        $counter = Apartman::where('type', Constant::APARTMAN_TYPE_APARTMAN)->count();

        return $counter ?: '';
    }

    public static function brojac_soba()
    {
        $counter = Apartman::where('type', Constant::APARTMAN_TYPE_SOBA)->count();

        return $counter ?: '';
    }

    public static function brojac_studio()
    {
        $counter = Apartman::where('type', Constant::APARTMAN_TYPE_STUDIO)->count();

        return $counter ?: '';
    }

    public static function brojac_kuca()
    {
        $counter = Apartman::where('type', Constant::APARTMAN_TYPE_KUCA)->count();

        return $counter ?: '';
    }
}