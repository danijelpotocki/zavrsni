<?php
namespace App\Zavrsni\Estate\Model\Estate;

use Illuminate\Database\Eloquent\Model;

class Details extends Model
{
    protected $table = 'real_estate_details';

    protected $fillable = ['real_estate_id' , 'distance_center', 'distance_store', 'distance_disco', 'distance_beach', 'distance_restoran', 'distance_transport',
        'distance_ferry_city', 'distance_ferry', 'distance_plan_city', 'distance_plane', 'car', 'transport', 'main_road', 'road_beach', 'near_beach', 'quiet', 'alone', 'green', 'pebble', 'sand', 'concrete'
        , 'rock', 'description'];

    public function estate()
    {
        return $this->belongsTo('AApp\Zavrsni\Estate\Model\Estate', 'id', 'real_estate_id');
    }
}