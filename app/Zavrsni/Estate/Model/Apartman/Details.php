<?php
namespace App\Zavrsni\Estate\Model\Apartman;

use Illuminate\Database\Eloquent\Model;

class Details extends Model
{
    protected $table = 'apartman_details';

    protected $fillable = ['apartman_id', 'tv', 'satellite', 'dvd', 'internet', 'radio', 'cooler', 'freezer', 'stove', 'stove2', 'barbacue',
        'aircondition', 'washer', 'sea_view', 'parking', 'garage', 'pool', 'terrace', 'breakfast', 'smoking', 'pet', 'disabled_person', 'tax', 'additional_price'
        , 'description', 'reserved'];


    public function apartman()
    {
        return $this->belongsTo('App\Zavrsni\Estate\Model\Apartman', 'id', 'apartman_id');
    }
}