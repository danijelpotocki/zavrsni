<?php

namespace App\Zavrsni\Estate\Model;


use Illuminate\Database\Eloquent\Model;

class Estate extends Model
{
    protected $table = 'real_estate';

    protected $fillable = ['user_id', 'name', 'address', 'postal_code', 'city_id', 'type', 'lat', 'long', 'description'];

    public function country()
    {
        return $this->belongsTo('App\Zavrsni\Account\Model\User', 'user_id', 'id');
    }

    public function getMinMaxPrice()
    {
        $estate_repository = \App::make('EstateRepository');
        return $estate_repository->getMinMaxPrice($this);
    }

    public function city()
    {
        return $this->belongsTo('App\Zavrsni\Location\Model\City', 'city_id', 'id');
    }

    public function details()
    {
        return $this->hasOne('App\Zavrsni\Estate\Model\Estate\Details', 'real_estate_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Zavrsni\Account\Model\User', 'user_id', 'id');
    }

    public function apartman(){
        return $this->hasMany('App\Zavrsni\Estate\Model\Apartman', 'real_estate_id', 'id');
    }

    public function media(){
        return $this->hasMany('App\Zavrsni\Media\Model\Media', 'real_estate_id', 'id');
    }
}