<?php

namespace App\Zavrsni\Estate\Model;


use Illuminate\Database\Eloquent\Model;

class Apartman extends Model
{
    protected $table = 'apartman';

    protected $fillable = ['real_estate_id', 'type', 'level', 'bed', 'room', 'price', 'provision', 'dates', 'people', 'minimum_people', 'price_type'];

    public function estate()
    {
        return $this->belongsTo('App\Zavrsni\Account\Model\Estate', 'real_estate_id', 'id');
    }

    public function details()
    {
        return $this->hasOne('App\Zavrsni\Estate\Model\Apartman\Details', 'apartman_id', 'id');
    }

    public function getBed1()
    {
        $bed = explode( '+', $this->bed);
        return $bed[0];
    }

    public function getBed2()
    {
        $bed = explode( '+', $this->bed);
        return $bed[1];
    }
}