<?php namespace App\Zavrsni\Estate;

use Illuminate\Support\ServiceProvider;

/**
 * Class ServiceProvider
 * @package Voxterr\Account
 */
class EstateServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Register the service with the IoC container.
     *
     * @return void
     */
    public function register()
    {
        $ns = 'App\Zavrsni\Estate';

        $this->app->bind('Apartman', $ns . '\Model\Apartman');
        $this->app->bind('ApartmanDetails', $ns . '\Model\Apartman\Details');
        $this->app->bind('Estate', $ns . '\Model\Estate');
        $this->app->bind('EstateDetails', $ns . '\Model\Estate\Details');
        $this->app->bind('EstateRepository', $ns . '\Repository\EstateRepository');
        $this->app->bind('ApartmanRepository', $ns . '\Repository\ApartmanRepository');
    }


    public function provides()
    {
        return [
            'Apartman',
            'ApartmanDetails',
            'Estate',
            'EstateDetails',
            'EstateRepository',
            'ApartmanRepository',
        ];
    }
}
