<?php
namespace App\Zavrsni\Estate\Repository;

use App;

class EstateRepository
{

    public function getMinMaxPrice($estate)
    {
        if (!$estate->apartman->count()) {
            return '';
        }

        $first_apartman = $estate->apartman->first();

        $priceType = $first_apartman->price_type;


        $price = $first_apartman->price;
        $price = explode(";", $price);

        $provision = $first_apartman->provision;

        $minprice = ceil($price[0] + $provision / 100 * $price[0]);
        $maxprice = ceil($price[0] + $provision / 100 * $price[0]);

        if ($priceType == 'cijena_osoba') {
            $person_number = $first_apartman->people;
            $person_number = explode(";", $person_number);
            $min_osoba = $person_number[0];
            $minprice *= $person_number[0];
            $maxprice *= $person_number[0];
        }

        foreach ($estate->apartman as $apartman) {
            $priceType = $apartman->price_type;

            $price = $apartman->price;
            $price = explode(";", $price);

            $provision = $apartman->provision;

            for ($k = 0; $k < count($price); $k++) {
                $price[$k] = ceil($price[$k] + $provision / 100 * $price[$k]);

                if ($priceType == 'cijena_osoba') {
                    $person_number = $apartman->people;
                    $person_number = explode(";", $person_number);
                    $price[$k] *= $person_number[$k];
                }
            }

            for ($k = 0; $k < count($price); $k++) {

                if ($price[$k] < $minprice AND $price[$k]) {
                    $minprice = $price[$k];
                }

                if ($price[$k] > $maxprice) {
                    $maxprice = $price[$k];
                    $person_number = $apartman->people;
                    $person_number = explode(";", $person_number);
                    $min_osoba = $person_number[$k];
                    $id = $apartman->id;
                }
            }
        }

        if (isset($id)) {
            if ($priceType == 'cijena_osoba') {
                $kapacitet = \App::make('Apartman')->find($id)->bed;
                $kapacitet = explode("+", $kapacitet);
                $kapacitet = $kapacitet[0] + $kapacitet[1];
                $maxprice = ($maxprice / $min_osoba) * $kapacitet;
            }
        }


        if ($minprice == $maxprice AND $minprice) {
            $printprice = $minprice . '€';
        } else {
            $printprice = $minprice . '€ - ' . $maxprice . '€';
        }

        if (!$minprice AND !$maxprice) {
            $printprice = 0;
        }

        return $printprice;
    }

    public function saveEstate($input, $estate = null)
    {
        $basic = $input['apartman']['basic'];
        $details = $input['apartman']['details'];

        $cityId = explode(',', $basic['city_id']);
        $cityId = $cityId[0];
        $basic['city_id'] = $cityId;

        $basic['type'] = 'private';
        $basic['user_id'] = \Auth::user()->id;

        if (!$estate) {
            $estate = App::make('Estate');
        }

        $estate->fill($basic);
        $estate->save();

        $details['car'] = isset($details['car']) ? 1 : 0;
        $details['transport'] = isset($details['car']) ? 1 : 0;
        $details['main_road'] = isset($details['car']) ? 1 : 0;
        $details['road_beach'] = isset($details['car']) ? 1 : 0;
        $details['near_beach'] = isset($details['car']) ? 1 : 0;
        $details['quiet'] = isset($details['car']) ? 1 : 0;
        $details['alone'] = isset($details['car']) ? 1 : 0;
        $details['green'] = isset($details['car']) ? 1 : 0;
        $details['pebble'] = isset($details['car']) ? 1 : 0;
        $details['sand'] = isset($details['car']) ? 1 : 0;
        $details['concrete'] = isset($details['car']) ? 1 : 0;
        $details['rock'] = isset($details['car']) ? 1 : 0;
        $details['real_estate_id'] = $estate->id;

        if (!$estate->details) {
            $estateDetails = App::make('EstateDetails');
        } else {
            $estateDetails = $estate->details;
        }

        $estateDetails->fill($details);
        $estateDetails->save();

        return $estate;
    }

    public function saveApartman($input, $apartman = null)
    {
        $basic = $input['apartman']['basic'];
        $details = $input['apartman']['details'];

        $basic['bed'] = $basic['bed1'] . '+' .$basic['bed2'];
        $basic['provision'] = 0;

        $apartman = App::make('Apartman');
        $apartman->fill($basic);
        $apartman->save();

        $details['tv'] = isset($details['car']) ? 1 : 0;
        $details['satellite'] = isset($details['car']) ? 1 : 0;
        $details['dvd'] = isset($details['car']) ? 1 : 0;
        $details['internet'] = isset($details['car']) ? 1 : 0;
        $details['radio'] = isset($details['car']) ? 1 : 0;
        $details['cooler'] = isset($details['car']) ? 1 : 0;
        $details['freezer'] = isset($details['car']) ? 1 : 0;
        $details['stove'] = isset($details['car']) ? 1 : 0;
        $details['stove2'] = isset($details['car']) ? 1 : 0;
        $details['barbecue'] = isset($details['car']) ? 1 : 0;
        $details['aircondition'] = isset($details['car']) ? 1 : 0;
        $details['washer'] = isset($details['car']) ? 1 : 0;
        $details['bathtub'] = isset($details['car']) ? 1 : 0;
        $details['shower'] = isset($details['car']) ? 1 : 0;
        $details['sea_view'] = isset($details['car']) ? 1 : 0;
        $details['parking'] = isset($details['car']) ? 1 : 0;
        $details['garage'] = isset($details['car']) ? 1 : 0;
        $details['pool'] = isset($details['car']) ? 1 : 0;
        $details['terrace'] = isset($details['car']) ? 1 : 0;
        $details['breakfast'] = isset($details['car']) ? 1 : 0;
        $details['smoking'] = isset($details['car']) ? 1 : 0;
        $details['pet'] = isset($details['car']) ? 1 : 0;
        $details['disabled_person'] = isset($details['car']) ? 1 : 0;
        $details['tax'] = isset($details['car']) ? 1 : 0;
        $details['additional_price'] = isset($details['car']) ? 1 : 0;

        $reserved = '';
        for ($i=0; $i<365; $i++){
            $reserved .= 0;
        }
        $details['reserved'] = $reserved;
        $details['apartman_id'] = $apartman->id;

        if (!$apartman->details) {
            $apartmanDetails = App::make('ApartmanDetails');
        } else {
            $apartmanDetails = $apartman->details;
        }

        $apartmanDetails->fill($details);
        $apartmanDetails->save();
    }
}