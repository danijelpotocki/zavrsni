<?php
namespace App\Zavrsni\Estate\Controller;


use App;
use App\Http\Controllers\Controller;
use App\Zavrsni\Location\Model\City;
use App\Zavrsni\Estate\Model\Estate;
use Input;

class EstateController extends Controller
{

    public function getMyestates()
    {
        return view('estate.my_estate');
    }

    public function getAddestate()
    {
        $cities = City::all();
        $estate = App::make('Estate');

        return view('estate.edit_estate', ['cities' => $cities, 'estate' => $estate]);
    }

    public function postAddestate()
    {
        $input = Input::all();

        $estateRepository = App::make('EstateRepository');
        $estate_id = $estateRepository->saveEstate($input)->id;

        return view('estate.edit_apartman', ['estate' => $estate_id]);
    }

    public function getEditestate()
    {
        $cities = City::all();

        return view('estate.edit_estate', ['cities' => $cities]);
    }

    public function postEditestate()
    {
        $input = Input::all();

        $estateId = $input['estate_id'];
        $estate = Estate::find($estateId);

        $estateRepository = App::make('EstateRepository');
        $estateRepository->saveEstate($input, $estate);

        return view('estate.edit_apartman');
    }

    public function getAddapartman($estate_id)
    {
        return view('estate.edit_apartman', ['estate' => $estate_id]);
    }

    public function postAddapartman()
    {
        $input = Input::all();

        $estateRepository = App::make('EstateRepository');
        $estateRepository->saveApartman($input);

        return view('estate.edit_apartman', ['estate' => $input['apartman']['basic']['real_estate_id']]);
    }

    public function getApartman($id)
    {
        $estate = Estate::find($id);

        if(!$estate) {
            return view('estate.not_available');
        }

        return view('estate.profile', ['estate' => $estate]);
    }
}