@extends('common.content')

@section('js_before')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_map.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_objects.css') }}" />

    <!-- DATE PICKER -->
    <!-- Copyright 2010 Itamar Arjuan -->
    <!-- jsDatePick is distributed under the terms of the GNU General Public License. -->
    <script type="text/javascript" src="{{ URL::asset('js/datepick_map.js') }}"></script>
@endsection

@section('js_after')
    <script type="text/javascript" src="{{ URL::asset('js/offer_slider.js') }}"></script>

    <!-- MAPS AND SIDEBARS -->
    <script type="text/javascript" src="{{ URL::asset('js/raphael.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/map-croatia.js') }}"></script>


    <script type="text/javascript">
        window.localStorage.setItem("border", 0);
        var slides = 3;

        function right(){
            var border = window.localStorage.getItem("border");
            if(border <= slides - 2){
                $(".slide").animate({left:"-=1161px"}, 500);
                border = +border + 1;
            }
            else if(border = slides - 1){
                $(".slide").animate({left:"0px"}, 500);
                border = 0;
            }
            window.localStorage.setItem("border", border);
        }

        function left(){
            var border = window.localStorage.getItem("border");
            if(border >= slides - 2){
                $(".slide").animate({left:"+=1161px"}, 500);
                border = +border - 1;
            }
            else if(border = -slides + 2 ){
                $(".slide").animate({left: (slides - 1) * -1161}, 500);
                border = 2 * slides - 4;
            }
            window.localStorage.setItem("border", border);
        }
    </script>
@endsection

@section('hide-show')
    <div class="slideshow_bottom_shadow"></div>

    <div class="slide_div">
        <div id="slideshow">

            <div class="slides">
                <ul>
                    <li id="slide-one">
                        <div class="slide_text top">Rezervirajte smještaj pozivom</div>
                        <div class="slide_text bottom">i ostvarite <span>5%</span> popusta!</div>
                        <img src="images/slide_photos/slide1.jpg"/>
                    </li>
                    <li id="slide-two">
                        <div class="slide_text top">Rezervirajte smještaj pozivom</div>
                        <div class="slide_text bottom">i ostvarite <span>5%</span> popusta!</div>
                        <img src="images/slide_photos/slide2.jpg"/>
                    </li>
                </ul>
            </div>

            <div class="slides_footer">
                <ul class="slides-nav">
                    <li class="on"><a href="#slide-one"></a></li>
                    <li><a href="#slide-two"></a></li>
                </ul>
            </div>

        </div>
    </div>

    <div id="main_search">
        <h2>Pretraga</h2>
        <div class="body">
            <div class="search_box">
                <h3>Mjesto</h3>
                <div class="styled-select index">
                    <select name="mjesto" id="mjesto_trazi" class="input search_index select">
                        <option value="" selected="selected">Regija, Grad, Otok...</option>
                        <optgroup label="Regija">
                        <option value="45.242020,13.960876,9,Istra">Istra</option>
                        <option value="44.801327,14.576111,9,Kvarner">Kvarner</option>
                        <option value="43.413029,16.537170,8,Dalmacija">Dalmacija</option>
                        <option value="43.854336,15.567627,9,Sjeverna Dalmacija">Sjeverna Dalmacija</option>
                        <option value="43.084937,16.762390,9,Srednja Dalmacija">Srednja Dalmacija</option>
                        <option value="42.771211,17.057648,9,Južna Dalmacija">Južna Dalmacija</option>
                        <option value="45.352145,16.899719,8,Kontinentalna Hrvatska">Kontinentalna Hrvatska</option>
                        <option value="44.735028,15.408325,10,Lika i Gorski Kotar">Lika i Gorski Kotar</option>
                        <option value="45.794340,15.965881,10,Zagreb i okolica">Zagreb i okolica</option>
                        <option value="46.048455,16.410828,10,Hrvatsko Zagorje">Hrvatsko Zagorje</option>
                        <option value="45.683158,17.737427,10,Slavonija i Baranja">Slavonija i Baranja</option>
                        </optgroup>

                            @foreach($regions as $region)
                                <optgroup label="{{ $region->name }}">
                                    @foreach($region->city as $city)
                                        <option value="{{ $city->lat.','.$city->long.','. $city->zoom_level.','.$city->name }}">{{ $city->name }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="search_box cal first">
                <h3>Dolazak</h3>
                <input type="text" value="" id="datepick11" onMouseOver="new JsDatePick(this.id,{})" class="input search_index calendar">
            </div>
            <div class="search_box cal last">
                <h3>Odlazak</h3>
                <input type="text" value="" id="datepick12" onMouseOver="new JsDatePick(this.id,{})" class="input search_index calendar">
            </div>
            <div class="search_box">
                <h3>Broj osoba</h3>
                <div class="styled-select index">
                    <select  id="osoba_trazi" class="input search_index select">
                        <option value="" selected="selected"></option>
                        <option value="1">1 osoba</option>
                        <option value="2">2 osobe</option>
                        <option value="3">3 osobe</option>
                        <option value="4">4 osobe</option>
                        <option value="5">5 osoba</option>
                        <option value="6">6 osoba</option>
                        <option value="7">7 osoba</option>
                        <option value="8">8 osoba</option>
                        <option value="9">9 osoba</option>
                        <option value="10">10 osoba</option>
                        <option value="11">11 osoba</option>
                        <option value="12">12 osoba</option>
                        <option value="13">13 osoba</option>
                        <option value="14">14 osoba</option>
                        <option value="15">15 osoba</option>
                        <option value="16">16 osoba</option>
                        <option value="17">17 osoba</option>
                        <option value="18">18 osoba</option>
                        <option value="19">19 osoba</option>
                        <option value="20">20 osoba</option>
                    </select>
                </div>
            </div>
            <div class="search_box_button">
                <div class="button" onclick="trazilica();" >Traži</div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="full">

    <div class="half left">
        <div class="content_area_header">Pretraži kartu</div>
        <div id="map_left">
            <a href="{{ URL::to('/map') }}?map=all-jd">
                <div class="location">Južna Dalmacija</div>
            </a>
            <a href="{{ URL::to('/map') }}?map=all-sd">
                <div class="location">Srednja Dalmacija</div>
            </a>
            <a href="{{ URL::to('/map') }}?map=all-sjd">
                <div class="location">Sjeverna Dalmacija</div>
            </a>
            <a href="{{ URL::to('/map') }}?map=all-is">
                <div class="location">Istra</div>
            </a>
            <a href="{{ URL::to('/map') }}?map=all-kv">
                <div class="location">Kvarner</div>
            </a>
            <a href="{{ URL::to('/map') }}?map=all-lk">
                <div class="location">Lika i Gorski Kotar</div>
            </a>
            <a href="{{ URL::to('/map') }}?map=all-zg">
                <div class="location">Zagreb i okolica</div>
            </a>
            <a href="{{ URL::to('/map') }}?map=all-sh">
                <div class="location">Središnja Hrvatska</div>
            </a>
            <a href="{{ URL::to('/map') }}map=all-sl">
                <div class="location">Slavonija</div>
            </a>
        </div>
        <div id="map_croatia" class="raphael">
            <div id="map" class="index"></div>
        </div>
    </div>

    <div class="half right">
        <div class="content_area_header">Kontaktirajte nas!</div>
        <div class="contact">
            <div class="contact_text">
                Imate pitanja ili probleme sa rezervacijom?
            </div>
            <div class="contact_text">
                <div class="icon contact_clock"></div>
                <div class="text header">korisnička podrška:</div>
                <div class="text">Pon-Sub 13:00-19:00</div>
            </div>
            <!--
                        <div class="contact_text">
                            <div class="icon contact_phone"></div>
                            <div class="text header">telefon:</div>
                            <div class="text">+385 95 8854 556</div>
                        </div>
            -->
            <div class="contact_text">
                <div class="icon contact_mail"></div>
                <div class="text header">email:</div>
                <div class="text">info@smjestajhrvatska.com</div>
            </div>
        </div>
    </div>
    </div>

@endsection

