<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Smještaj Hrvatska | Privatni Smještaj | Apartmani, Sobe, Studio Apartmani i Kuće</title>


    <!-- STYLE CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_structure.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_nav_footer.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_map.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_content.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_objects.css') }}" />

    <!-- FUNCTIONS -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js?ver=2.9.2"></script>
    <script type="text/javascript" src="{{ URL::asset('widgets/jquery.cycle.js') }}"></script>

</head>
{{--<body style="float:left; margin:0px; width:1098px; background:none;" onLoad="if_gmap_init()">--}}

<div id="add_object_accommodation_iframe">
    <div class="full">

        <div class="title">{{ Auth::user()->name . ' ' . Auth::user()->surname }}</div>
        <div class="text">Država: <span>{{ Auth::user()->country->name  }}</span></div>
        <div class="text">Telefon: <span>{{ Auth::user()->phone  }}</span></div>
        <div class="text" style="margin-bottom:20px;">E-mail adresa: <span>{{ Auth::user()->email }}</span></div>
        <a class="button" style href="{{ URL::to('account/edit') }}" type="submit" name="submit">Promijeni</a>

    </div>
</div>

</body>
</html>