@extends('common.content')

@section('js_before')
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var elements = document.getElementsByTagName("INPUT");
            for (var i = 0; i < elements.length; i++) {
                elements[i].oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        e.target.setCustomValidity("Polja označena zvijezdicom su obavezna.");
                    }
                };
                elements[i].oninput = function(e) {
                    e.target.setCustomValidity("");
                };
            }

            var selements = document.getElementsByTagName("SELECT");
            for (var i = 0; i < selements.length; i++) {
                selements[i].oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        e.target.setCustomValidity("Polja označena zvijezdicom su obavezna.");
                    }
                };
                selements[i].onchange = function(e) {
                    e.target.setCustomValidity("");
                };
            }
        })
    </script>
@endsection

@section('content')
    <script>
        function checkpass(){
            if(pass.value.length<6){
                alert("Vaša lozinka mora imati barem 6 znakova!");
                pass.focus();
                return false;
            }

            if (pass.value != cpass.value) {
                alert("Vaša lozinka i potvrđena lozinka nisu jednake!");
                cpass.focus();
                return false;
            }
        }
    </script>

    <div class="full">
        <div class="content_area_header">Besplatno oglašavanje privatnog smještaja, apartmana, soba i ostalog turističkog sadržaja u Hrvatskoj</div>
        {!! Form::open(['autocomplete' => 'off', 'onsubmit' => 'return checkpass();']) !!}
            <table class="table" style="width:710px; margin-top:30px;">
                <tr>
                    <td><a><label for="title">Titula:</label></a></td>
                    <td><a><label for="country">Država:</label></a> <em>*</em></td>
                </tr>
                <tr>
                    <td><select class="input" name="title" id="title" tabindex="1">
                            <option selected="selected" value="0"></option>
                            <option>Gosp.</option>
                            <option>Gđa.</option>
                            <option>Gđica.</option>
                        </select></td>
                    <td><select class="input" name="country_id" id="country" required="required" tabindex="5" style="width:293px; height:24px">
                            @foreach($countries as $country)
						        <option value="{{ $country->id }}" {{ $country->id == 61 ? 'selected' : '' }}>{{ $country->name }}</option>';
                            @endforeach
                        </select></td>
                </tr>
                <tr>
                    <td><a><label for="name">Ime:</label></a><em>*</em></td>
                    <td><a><label for="email">E-mail adresa:</label></a><em>*</em></td>
                </tr>
                <tr>
                    <td><input class="input" id="name" name="name" required="required" type="text" size="45" tabindex="2"/></td>
                    <td><input class="input" id="email" name="email" required="required" type="email" size="45" tabindex="6"/></td>
                </tr>
                <tr>
                    <td><a><label for="surname">Prezime:</label></a><em>*</em></td>
                    <td><a><label for="pass">Lozinka:</label></a> <em>*</em></td>
                </tr>
                <tr>
                    <td><input class="input" id="surname" name="surname" required="required" type="text" size="45" tabindex="3"/></td>
                    <td><input class="input" id="pass" name="password" required="required" type="password" size="45" tabindex="7"/></td>
                </tr>
                <tr>
                    <td><a><label for="phone">Telefon:</label></a><em>*</em></td>
                    <td><a><label for="cpass">Potvrdite lozinku:</label></a><em>*</em></td>
                </tr>
                <tr>
                    <td><input class="input" id="phone" name="phone" required="required" type="text" size="45" tabindex="4"/></td>
                    <td><input class="input" id="cpass" name="password_confirmation" required="required" type="password" size="45" tabindex="8"/></td>
                </tr>

                @if(isset($errors))
                    @foreach($errors as $error)
                    <tr>
                        <td><font color="red">{{ $error }}</font></td>
                    </tr>
                    @endforeach
                @endif
                <tr>
                    <td><input class="button reg" type="submit" value="Registriraj se" name="submit" /></td>
                </tr>
            </table>
        {!! Form::close() !!}
    </div>

    <div class="full">
        <div class="header_why">Zašto oglašavati na SmjestajHrvatska.com mreži kataloga?</div>
        <div class="text_why">Jer SmještajHrvatska.com nudi svakome da promovira i prodaje svoju turističku ponudu on-line!</div>
    </div>

    <div class="full">
        <div class="header_why">Cjenik Oglašavanja</div>
        <div class="text_why">OGLAŠAVANJE JE POTPUNO BESPLATNO!</div>
    </div>

    <div class="full">
        <div class="header_why">Gdje će se sve prikazivati Vaš oglas</div>
        <div class="text_why">Vaš oglas prikazivati će se na SmještajHrvatska.com, te istoj stranici prevedenoj na njemački i engleski jezik.</div>
    </div>

    <div class="full">
        <div class="header_why">Proces do objave oglasa</div>
        <div class="text_why">SmještajHrvatska.com zaprimit će i obraditi svaki oglas u najkraćem mogućem roku te provjeriti njegovu vjerodostojnost kako bi stvorila sigurno okruženje za kupnju na svojim stranicama.</div>
    </div>

    <div class="full">
        <div class="header_why">Što je uključeno u svaki oglas</div>
        <div class="text_why">Svaki oglas uključuje pružanje informacija o vlastitoj ponudi registriranih korisnika te dodavanje slika.</div>
    </div>

    {{--<div class="full">--}}
        {{--<div class="header_why">Pomoć i Podrška</div>--}}
        {{--<div class="text_why">E-mail: kontakt@smjestajhrvatska.com<br />Telefon: +385 95 8854556</div>--}}
    {{--</div>--}}

    <div class="directions" style="top:310px;">
        <div class="header">
            Upute za registraciju oglašivača:
        </div>
        <div class="text active">
            <span>1.</span>	Ispunite svoje osobne podatke i odaberite Registriraj se
        </div>
        <div class="text">
            <span>2.</span>	Popunite podatke za Vaš objekt i odaberite Spremi na dnu stranice
        </div>
        <div class="text">
            <span>3.</span> Ispunite informacije za pojedinačnu smještajnu jedinicu unutar objekta (za svaki apartman, sobu, studio apartman, kuću itd.) i odaberite Spremi
        </div>
        <div class="text">
            <span>4.</span>	Ponovite postupak iz točke 3. dok ne dodate sve pojedinačne jedinice unutar istog objekta
        </div>
        <div class="text">
            <span>5.</span>	Novi objekt dodajte pritiskom na rubriku Dodaj objekt
        </div>
    </div>
@endsection