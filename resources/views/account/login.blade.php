@extends('common.content')

@section('content')


{!! Form::open() !!}

<div class="header_why">Prijavite se</div>
<table class="table" style="margin:10px 0px 40px 0px">
    @if(isset($error))
                <tr>
                    <td><a style="color:#de543e; margin:0px 0px 10px 0px; float:left;">{{ $error }}</a></td>
                </tr>
    @endif
    <tr>
        <td><a><label>Vaš e-mail:</label></a></td>
    </tr>
    <tr>
        <td><input class="input" id="email" name="email" required="required" type="text" size="35" /></td>
    </tr>
    <tr>
        <td><a><label>Vaša Lozinka:</label></a></td>
    </tr>
    <tr>
        <td><input class="input" id="password" name="password" required="required" type="password" size="35" /></td>
    </tr>
    <tr>
        <td><input class="button" style="float:left;" type="submit" value="Prijavi se" name="submit" />
            <a style="margin:10px 0px 0px 10px; float:left; color:#64c558; text-decoration:underline;" href="{{ URL::to('account/forgotten') }}">Zaboravili ste lozinku?</a></td>
    </tr>
    <tr>
    </tr>
</table>
{!! Form::close() !!}

<div class="header_why">Niste se Registrirali? Besplatno je!</div>
<form action="{{ URL::to('account/register') }}">
    <input class="button" type="submit" value="Registriraj se">
</form>
@endsection