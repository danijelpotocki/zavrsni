@extends('common.content')

@section('js_before')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_objects.css') }}" />
    <script>
        function change_url(url){
            window.location.href=url;
        }
    </script>
    <!-- DATE PICKER -->
    <!-- Copyright 2010 Itamar Arjuan -->
    <!-- jsDatePick is distributed under the terms of the GNU General Public License. -->
    <script type="text/javascript" src="{{ URL::asset('js/datepick.js') }}"></script>
@endsection

@section('content')

    <?php
    $page = Input::get('page');

    $mo = '';
    $do = '';
    $profil = '';
    if($page == 'mo'){
        $mo = 'active';
    }elseif($page == 'do'){
        $do = 'active';
    }else{
        $profil = 'active';
    }

    ?>
    <script type="text/javascript">

    $(window).load(function() {

        $("#profile_tab_0").click(function () {
            var ifr = document.getElementsByName('profil')[0];
            ifr.src = ifr.src;
            $(".active").removeClass("active");
            $("#profile_menu_1").hide();
            $("#profile_menu_2").hide();
            $("#profile_tab_0").addClass("active");
            $("#profile_menu_0").show();
        });

        $("#profile_tab_1").click(function () {
            var ifr = document.getElementsByName('mojiobjekti')[0];
            ifr.src = ifr.src;
            $(".active").removeClass("active");
            $("#profile_menu_0").hide();
            $("#profile_menu_2").hide();
            $("#profile_tab_1").addClass("active");
            $("#profile_menu_1").show();
        });

        $("#profile_tab_2").click(function () {
            var ifr = document.getElementsByName('dodajobjekt')[0];
            ifr.src = ifr.src;
            $(".active").removeClass("active");
            $("#profile_menu_1").hide();
            $("#profile_menu_0").hide();
            $("#profile_tab_2").addClass("active");
            $("#profile_menu_2").show();
        });

    });

    function izbrisi(value,id){
        $(".close_bg").fadeIn(250);
        $(".delete_confirm").fadeIn(250);
        $('#izbrisi').load('../config/delete_div.php?value='+value+'&id='+id);
    }

    function ugasi() {
        $(".delete_confirm").fadeOut(250);
        $(".close_bg").fadeOut(250);
    }


    function resizeIframe(obj)
    {

        obj.style.height = obj.contentWindow.document.body.scrollHeight + 10 + 'px';
    }

</script>


<div class="profile_tab <?php echo ($profil ? 'active' : "");?>" id="profile_tab_0">
    <div class="title">Profil</div>
    <div class="hide_shadow"></div>
</div>

<div class="profile_tab <?php echo ($mo ? 'active' : "");?>" id="profile_tab_1">
    <div class="title">Moji Objekti</div>
    <div class="hide_shadow"></div>
</div>

<div class="profile_tab <?php echo ($do ? 'active' : "");?>" id="profile_tab_2">
    <div class="title">Dodaj Objekt</div>
    <div class="hide_shadow"></div>
</div>


<div class="profile_menu">

    <div class="profile_menu_hide" id="profile_menu_0" <?php echo ($profil ? 'style="display:block;"' : "");?>>
        <iframe name="profil" style="width:1100px; border:none; overflow:hidden;" src="{{ URL::to('account/profil') }}" onload="resizeIframe(this);"></iframe>
    </div>

    <div class="profile_menu_hide" id="profile_menu_1" <?php echo ($mo ? 'style="display:block;"' : "");?>>
        <iframe name="mojiobjekti" id="mojiobjekti" style="width:1100px; border:none; overflow:hidden;" src="{{ URL::to('estate/myestates') }}" onload="resizeIframe(this);"></iframe>
    </div>

    <div class="profile_menu_hide" id="profile_menu_2" <?php echo ($do ? 'style="display:block;"' : "");?>>
        <iframe name="dodajobjekt" id="dodajobjekt" style="width:1100px; border:none; overflow:hidden;" src="{{ URL::to('estate/addestate') }}" onload="resizeIframe(this);"></iframe>
    </div>

</div>
@endsection