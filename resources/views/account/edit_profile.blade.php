<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Smještaj Hrvatska | Privatni Smještaj | Apartmani, Sobe, Studio Apartmani i Kuće</title>


    <!-- STYLE CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_structure.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_nav_footer.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_map.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_content.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_objects.css') }}" />




    <!-- FUNCTIONS -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js?ver=2.9.2"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.cycle.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var elements = document.getElementsByTagName("INPUT");
            for (var i = 0; i < elements.length; i++) {
                elements[i].oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        e.target.setCustomValidity("Polja označena zvijezdicom su obavezna.");
                    }
                };
                elements[i].oninput = function(e) {
                    e.target.setCustomValidity("");
                };
            }

            var selements = document.getElementsByTagName("SELECT");
            for (var i = 0; i < selements.length; i++) {
                selements[i].oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        e.target.setCustomValidity("Polja označena zvijezdicom su obavezna.");
                    }
                };
                selements[i].onchange = function(e) {
                    e.target.setCustomValidity("");
                };
            }
        })
    </script>

</head>
<body style="float:left; margin:0px; width:840px; background:none;">

<div id="add_object_accommodation_iframe">
    <div class="full">

        <div class="title">Promijeni podatke</div>
        {!! Form::open() !!}
            <table class="table" style=" margin-bottom:50px;">
                <tr>
                    <td><a><label for="title">Titula:</label></a> </td>
                </tr>
                <tr>
                    <td><select class="input" name="title" id="title" tabindex="1">
                            <option <?php echo (Auth::user()->title == "0" ? 'selected="selected" value="0"' : 'value="0"');?> ></option>
                            <option <?php echo (Auth::user()->title == "Gosp." ? 'selected="selected"' : "");?>>Gosp.</option>
                            <option <?php echo (Auth::user()->title == "Gđa." ? 'selected="selected"' : "");?>>Gđa.</option>
                            <option <?php echo (Auth::user()->title == "Gđica." ? 'selected="selected"' : "");?>>Gđica.</option>
                        </select></td>
                </tr>
                <tr>
                    <td><a><label for="name">Ime:<em>*</em></label></a></td>
                </tr>
                <tr>
                    <td><input class="input" id="country" name="name" required type="text" size="35" tabindex="2" value="{{ Auth::user()->name }}"/></td>
                </tr>
                <tr>
                    <td><a><label for="surname">Prezime:<em>*</em></label></a></td>
                </tr>
                <tr>
                    <td><input class="input" id="country" name="surname" required type="text" size="35" tabindex="3" value="{{ Auth::user()->surname }}"/></td>
                </tr>
                <tr>
                    <td><a><label for="country">Država:<em>*</em></label></a></td>
                </tr>
                <tr>
                    <td><select class="input" name="country_id" id="country" required="required" tabindex="4" style="width:250px; height: 24px">
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ $country->id == Auth::user()->country_id ? 'selected' : '' }}>{{ $country->name }}</option>';
                            @endforeach
                        </select></td>
                <tr>
                </tr>
                <td><a><label for="phone">Telefon:<em>*</em></label></a></td>
                </tr>
                <tr>
                    <td><input class="input" id="phone" name="phone"  required="required" type="text" size="35" tabindex="5" value="{{ Auth::user()->phone }}"/></td>
                </tr>
                <tr>
                    <td><a><label for="email">E-mail adresa:<em>*</em></label></a></td>
                </tr>
                <tr>
                    <td><input class="input" id="email" name="email"  required="required" type="email" size="35" tabindex="6" value="{{ Auth::user()->email }}"/></td>
                </tr>
                <tr>
                    <td><input class="button" type="submit" value="Promijeni" name="submit" /></td>
                </tr>
            </table>
        {!! Form::close() !!}



        <script>
            function checkpass(){
                if (pass.value != cpass.value) {
                    alert("Vaša lozinka i potvrđena lozinka nisu jednake!");
                    cpassword.focus();
                    return false;
                }
            }
        </script>

        <div class="title">Promijeni lozinku</div>
        {!! Form::open([ 'url' => URL::to('account/changepassword'), 'onsubmit' => 'onSubmit="return checkpass()"']) !!}
            <table class="table">
                <tr>
                    <td><a><label for="old_pass">Stara lozinka:<em>*</em></label></a></td>
                </tr>
                <tr>
                    <td><input class="input" style="float:left;" id="old_password" name="old_pass" required type="password" size="35" tabindex="7"/>
                        @if(isset($password_errors))
                            @foreach($password_errors as $error)
                                <a style="color:#dc0000; margin:2px 0px 0px 10px; float:left;">{{ $error }}</a>
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <td><a><label for="pass">Nova lozinka:<em>*</em></label></a></td>
                </tr>
                <tr>
                    <td><input class="input" id="pass" name="password" required type="password" size="35" tabindex="8"/></td>
                </tr>
                <tr>
                    <td><a><label for="cpass">Potvrdite lozinku:<em>*</em></label></a></td>
                </tr>
                <tr>
                    <td><input class="input" id="cpass" name="password_confirmation"  required="required" type="password" size="35" tabindex="9"/></td>
                </tr>
                <tr>
                    <td><input class="button" type="submit" value="Promijeni" name="submit" /></td>
                </tr>
            </table>
        {!! Form::close() !!}

    </div>
</div>

</body>
</html>