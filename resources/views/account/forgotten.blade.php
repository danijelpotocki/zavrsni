@extends('common.content')


@section('content')

<div class="full">

    {!! Form::open() !!}
        <div class="header_why">Zaboravili ste svoju lozinku? Upišite svoj e-mail na koji će vam doći nova lozinka!</div>
        <table class="table">
            <tr>
                <td><a><label for=email">Vaš e-mail:</label></a> <em>*</em></td>
            </tr>
            <tr>
                <td><input class="input" id="email" name="email" required="required" type="email" size="35"/>
                    @if(isset($error))
                        <div style="color:#de543e; margin-left:10px; display:inline;">{{ $error }}</div></td>
                    @endif
            </tr>
            <tr>
                <td><input class="button reg" type="submit" value="Dodjeli novu lozinku" name="submit" /></td>
            </tr>
        </table>
    {!! Form::close() !!}

    @if(isset($success))
        <div class="header_why" style="border-bottom:0px;">{{ $success }}</div>
    @endif
</div>
@endsection