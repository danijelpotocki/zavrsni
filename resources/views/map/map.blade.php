@extends('common.content')

@section('js_before')
    <link rel="stylesheet" type="text/css" href="{{ URL::to('style/style_map.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::to('style/style_objects.css') }}" />

    <!-- DATE PICKER -->
    <script type="text/javascript" src="{{ URL::to('js/datepick_list.js') }}"></script>
@endsection

@section('js_after')
        <!-- SIDEBAR, MAP, LIST -->
    <script type="text/javascript" src="{{ URL::to('js/raphael.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/map-croatia.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/list_generate.js') }}"></script>

    <!-- GOOGLE MAPS -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="{{ URL::to('js/markerclusterer.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/gmaps.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/infobox.js') }}"></script>
@endsection

@section('content')
    <div id="teasers">
        <div style="display:block;" class="roundteaser cat">


            <!----------------------- SIDEBAR  ---------------------------->
            <form action="#">
                <div id="sidebar">

                    <div class="header main">Pretraga</div>

                    <div id="sidebar_in">
                        <ul>
                            <li>
                                <div class="styled-select search">
                                    <select name="mjesto" id="mjesto_trazi" class="input search_index select">
                                        <option value="" selected="selected">Regija, Grad, Otok...</option>
                                        <optgroup label="Regija">
                                            <option value="45.242020,13.960876,9,Istra">Istra</option>
                                            <option value="44.801327,14.576111,9,Kvarner">Kvarner</option>
                                            <option value="43.413029,16.537170,8,Dalmacija">Dalmacija</option>
                                            <option value="43.854336,15.567627,9,Sjeverna Dalmacija">Sjeverna Dalmacija</option>
                                            <option value="43.084937,16.762390,9,Srednja Dalmacija">Srednja Dalmacija</option>
                                            <option value="42.771211,17.057648,9,Južna Dalmacija">Južna Dalmacija</option>
                                            <option value="45.352145,16.899719,8,Kontinentalna Hrvatska">Kontinentalna Hrvatska</option>
                                            <option value="44.735028,15.408325,10,Lika i Gorski Kotar">Lika i Gorski Kotar</option>
                                            <option value="45.794340,15.965881,10,Zagreb i okolica">Zagreb i okolica</option>
                                            <option value="46.048455,16.410828,10,Hrvatsko Zagorje">Hrvatsko Zagorje</option>
                                            <option value="45.683158,17.737427,10,Slavonija i Baranja">Slavonija i Baranja</option>
                                        </optgroup>

                                        @foreach($regions as $region)
                                            <optgroup label="{{ $region->name }}">
                                                @foreach($region->city as $city)
                                                    <option value="{{ $city->lat.','.$city->long.','. $city->zoom_level.','.$city->name }}">{{ $city->name }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </li>
                            <li>
                                <input type="text" placeholder="dolazak" id="datepick13" onMouseOver="new JsDatePick(this.id,{})" onkeyup="SearchDelay();" class="input sidebar_in calendar" style="width:85px; font-size:12px; padding:3px 2px 3px 2px;" > - <input type="text" placeholder="odlazak" id="datepick14" onMouseOver="new JsDatePick(this.id,{})" onkeyup="SearchDelay(1);" class="input sidebar_in calendar" style="width:85px; font-size:12px; padding:3px 2px 3px 2px;" >
                            </li>
                            <li>
                                <div class="styled-select search">
                                    <select  id="osoba" onchange="pretraga();" class="input sidebar_in select" style="font-size:12px;">
                                        <option value="" selected="selected">Broj osoba</option>
                                        <option value="1">1 osoba</option>
                                        <option value="2">2 osobe</option>
                                        <option value="3">3 osobe</option>
                                        <option value="4">4 osobe</option>
                                        <option value="5">5 osoba</option>
                                        <option value="6">6 osoba</option>
                                        <option value="7">7 osoba</option>
                                        <option value="8">8 osoba</option>
                                        <option value="9">9 osoba</option>
                                        <option value="10">10 osoba</option>
                                        <option value="11">11 osoba</option>
                                        <option value="12">12 osoba</option>
                                        <option value="13">13 osoba</option>
                                        <option value="14">14 osoba</option>
                                        <option value="15">15 osoba</option>
                                        <option value="16">16 osoba</option>
                                        <option value="17">17 osoba</option>
                                        <option value="18">18 osoba</option>
                                        <option value="19">19 osoba</option>
                                        <option value="20">20 osoba</option>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <input type="text" id="price_min" placeholder="Min. cijena u €" onkeyup="SearchDelay();" class="input sidebar_in" style="width:85px; font-size:12px; padding:3px 2px 3px 2px;" >
                                -
                                <input type="text" id="price_max" placeholder="Max. cijena u €" onkeyup="SearchDelay();" class="input sidebar_in" style="width:85px; font-size:12px; padding:3px 2px 3px 2px;" >
                            </li>
                            <li>
                                <div class="styled-select search">
                                    <select name="room" type="room" id="room" onchange="pretraga();" class="input sidebar_in select" style="font-size:12px;">
                                        <option value="" selected="selected">Broj spavaćih soba</option>
                                        <option value="1">1 spavaća soba</option>
                                        <option value="2">2 spavaće sobe</option>
                                        <option value="3">3 spavaće sobe</option>
                                        <option value="4">4 spavaće sobe</option>
                                        <option value="5">5 spavaćih soba</option>
                                    </select>
                                </div>
                            </li>
                        </ul>
                    </div>


                    <!-- brisi box 9, 16 -->
                    <div id="sidebar_in">
                        <ul>
                            <li class="header">Vrsta smještaja</li>
                            <li>
                                <input id="box_1" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_1" class="label">Apartmani</label>
                            </li>
                            <li>
                                <input id="box_2" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_2" class="label">Soba</label>
                            </li>
                            <li>
                                <input id="box_3" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_3" class="label">Studio</label>
                            </li>
                            <li>
                                <input id="box_4" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_4" class="label">Kuća za odmor</label>
                            </li>
                        </ul>
                    </div>

                    <div id="sidebar_in">
                        <ul>
                            <li class="header">Posebne ponude</li>
                            <li>
                                <input id="akcija" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="akcija" class="label">6+1 dan gratis</label>
                            </li>
                        </ul>
                    </div>

                    <div id="sidebar_in">
                        <ul>
                            <li class="header">Oprema i usluge</li>
                            <li>
                                <input id="box_6" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_6" class="label">Pogled na more</label>
                            </li>
                            <li>
                                <input id="box_7" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_7" class="label">Klima</label>
                            </li>
                            <li>
                                <input id="box_8" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_8" class="label">TV</label>
                            </li>
                            <li>
                                <input id="box_10" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_10" class="label">Parkirno mjesto</label>
                            </li>
                            <li>
                                <input id="box_11" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_11" class="label">Bazen</label>
                            </li>
                            <li>
                                <input id="box_12" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_12" class="label">Hladnjak</label>
                            </li>
                            <li>
                                <input id="box_13" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_13" class="label">Štednjak</label>
                            </li>
                            <li>
                                <input id="box_14" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_14" class="label">Roštilj</label>
                            </li>
                            <li>
                                <input id="box_15" onClick="pretraga();" class="checkbox" type="checkbox">
                                <label for="box_15" class="label">Kućni ljubimci</label>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>


            <div id="map_croatia">
                <div class="map_loading"></div>
                <div style="display:none;" id="map"></div>
                <div class="roundteaser cat">
                    <div id="map_canvas"></div>
                </div>
            </div>


            <div id="list">
                <div class="list_in search">
                    <div id="lista">
                        <script type="text/javascript">
                            @foreach($estates as $estate)
                                    $(document).ready(function() {
                                        if ( $(".accommodation").length ) {
                                            $("#ph{{ $estate->id }} > #photos").hover(function() {
                                                $(".active","#ph{{ $estate->id }}").removeClass("active");
                                                $($(".photo",$(this))).addClass("active");
                                                $($(".side_photo",$(this))).addClass("active");
                                            });
                                        }
                                    });
                            @endforeach
                        $(document).ready(function() {
                            if ( $(".accommodation").length ) {
                                $("#ph > #photos").hover(function() {
                                    $(".active","#$ph").removeClass("active");
                                    $($(".photo",$(this))).addClass("active");
                                    $($(".side_photo",$(this))).addClass("active");
                                });
                            }
                        });
                        </script>

                        @if(!$estates->count())
                        <div id="nav_search_private">
                            <div class="list_nav_text no_result">Nema objekata sa navedenim uvjetima.</div>
                        </div>
                        @else
                            @foreach($estates as $estate)
								<div id="object_search_private">
									<div class="header">
										 <div class="name">{{ $estate->name }}<span>({{ $estate->city->name }})</span></div>
										 <div class="img beach" title="Udaljenost od plaže">{{ $estate->details->distance_beach }}</div>
									</div>
									<div class="content">
										<div class="left">
											<ul id="so{{ $estate->id }}" class="accommodation">

                                @foreach($estate->apartman as $apartman)
											<a target="_blank"  href="{{ URL::to('estate/apartman') }}/{{ $estate->id }}">
												<li>
													<div class="text"><span>{{ $apartman->type }}</span> ({{ $apartman->bed }})</div>
	                                            </li>
											</a>
                                @endforeach
                                        </ul>
										<div id="ph{{ $estate->id }}" class="photos">


                                            <div id="photos">
                                                <div class="side_photo active">
													<img src="{{ URL::to('images/no_photo.jpg') }}" width="100%" height="100%" />
												</div>
                                                <div class="photo active">
													<img src="{{ URL::to('images/no_photo.jpg') }}" width="100%" height="100%" />
												</div>
											</div>
                                    	</div>

										<div class="left_price">
											<span>{{ $estate->getMinMaxPrice() }}</span>/noć
										</div>

                                    	<a class="more_button" target="_blank" href="{{ URL::to('estate/apartman') }}/{{ $estate->id }}">Detaljnije</a>
									</div>
								</div>

							</div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- InstanceEndEditable -->
@endsection