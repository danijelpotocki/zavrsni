<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Smještaj Hrvatska | Privatni Smještaj | Apartmani, Sobe, Studio Apartmani i Kuće</title>


    <!-- STYLE CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_structure.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_nav_footer.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_map.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_content.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_objects.css') }}" />

    <!-- FUNCTIONS -->
    <script type="text/javascript" src="{{ URL::asset('js/if_gmap.js') }}"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

</head>



<body style="float:left; margin:0px; width:1098px; background:none;">



<script type="text/javascript">
    $(document).ready(function() {
        if ( $(".accommodation").length ) {
            $("#ph > #photos").hover(function() {
                $(".active","#ph").removeClass("active");
                $($(".photo",$(this))).addClass("active");
                $($(".side_photo",$(this))).addClass("active");
            });
        }
    });
</script>

@if(Auth::user()->estate->count())
@foreach(Auth::user()->estate as $estate)

<div id="object_my">
    <div class="header">
        <div class="name">{{ $estate->name }} <span>({{ $estate->city->name }})</span></div>
        <div class="options" onClick="parent.izbrisi(0,'.$smjestajId.')">Izbriši</div>
        <div class="options" onClick="window.location = '{{ URL::to('estate/editestate') }}?estate={{ $estate->id }}';">Promijeni</div>
    </div>
    <div class="content">
        <div class="left">
            <ul id="so<?php  echo isset($i) ? $i++ : 0; ?>" class="accommodation">
                @foreach($estate->apartman as $apartman)
                <a>
                    <li>
                        <div class="text">
                            <span><?php echo $apartman->type; ?></span> ({{ $apartman->bed }})
                            <div class="options" onClick="parent.izbrisi(1,{{ $apartman->id }})">Izbriši</div>
                            <div class="options" onClick="window.location = '{{ URL::to('estate/editapartman') }}?apartman={{ $apartman->id }}'">Promijeni</div>
                        </div>
                    </li>
                </a>
                @endforeach
            </ul>

            <div id="ph" class="photos">

                <?php $i = 0; foreach($estate->media as $media){
                if($i >= 5)
                    break;
                ?>
                <div id="photos">
                    <div class="side_photo active">
                        <img width="100%" height="100%" src="{{ URL::asset('images/media/' . $media->name . '.jpg') }}" alt="" />
                    </div>
                    <div class="photo active">
                        <img width="100%" height="100%" src="{{ URL::asset('images/media/' . $media->name. '.jpg') }}" alt="" />
                    </div>
                </div>
                <?php } ?>

            </div>

            <div class="left_price">
                <span>{{ $estate->getMinMaxPrice() ?: "Na upit" }}</span>/noć
            </div>

            <a class="more_button" target="_blank" href="{{ URL::asset('estate/') }}?objekt={{ $estate->id }}" >Detaljnije</a>
            <a class="more_button" href="dodaj_objekt_iframe_2.php?objekt=<?php  echo $estate->id; ?>" >Dodaj smještaj</a>
        </div>
    </div>
</div>

@endforeach
@else


<div id="add_object_accommodation_iframe">
    <div class="full">
        <div class="title" style="margin-bottom:10px;">Nemate dodanih Objekata!</div>
        <a onClick="parent.change_url('{{ URL::to('account/') }}?page=do');" class="button">Dodaj objekt</a>
    </div>
</div>

@endif

</body>
</html>