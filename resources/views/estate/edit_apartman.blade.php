<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Smještaj Hrvatska | Privatni Smještaj | Apartmani, Sobe, Studio Apartmani i Kuće</title>


    <!-- STYLE CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_structure.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_nav_footer.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_map.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_content.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_objects.css') }}" />




    <!-- FUNCTIONS -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js?ver=2.9.2"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.cycle.js') }}"></script>

    <!-- DATE PICKER -->
    <!-- Copyright 2010 Itamar Arjuan -->
    <!-- jsDatePick is distributed under the terms of the GNU General Public License. -->
    <script type="text/javascript" src="{{ URL::asset('js/datepick.js') }}"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>

    <script type="text/javascript" src="{{ URL::asset('js/cjenik.js') }}"></script>
    <script>
        function validateForm(){
            var vrsta = document.forms["podaci"]["vrsta"].value;
            var pozicija = document.forms["podaci"]["pozicija"].value;
            var lezaj = document.forms["podaci"]["lezaj"].value;
            var room = document.forms["podaci"]["room"].value;
            var datum1 = document.forms["podaci"]["datum1"].value;
            var datum2 = document.forms["podaci"]["datum2"].value;
            var min1 = document.forms["podaci"]["min1"].value;
            var price1 = document.forms["podaci"]["price1"].value;

            if ((vrsta==null || vrsta=="") || (pozicija==null || pozicija=="") || (lezaj==null || lezaj=="") || (room==null || room=="")){
                alert("Molimo ispunite sva polja označena zvjezdicom.");
                return false;
            }

            if((datum1==null || datum1=="") || (datum2==null || datum2=="") || (min1==null || min1=="") || (price1==null || price1=="")){
                var br = 1;
            }

            if(document.getElementById('cijena_osoba').checked){
                var osoba1 = document.forms["podaci"]["osoba1"].value;

                if(osoba1==null || osoba1==""){
                    alert("Molimo ispunite podatke o cijenama.");
                    return false;
                }

            }else if(document.getElementById('cijena_napredno').checked){
                var price2 = document.forms["podaci"]["price2"].value;
                var price3 = document.forms["podaci"]["price3"].value;

                if((price2==null || price2=="") || (price3==null || price3=="")){
                    alert("Molimo ispunite podatke o cijenama.");
                    return false;
                }
            }
        }
    </script>
</head>
<body style="float:left; margin:0px; width:1098px; background:none; position:relative;">



<script type="text/javascript">
    $(document).ready(function() {
        if ( $(".accommodation").length ) {
            $("#smy0 > li").click(function() {
                $(".active", "#smy0").removeClass("active");
                $($(".right_switch",$(this))).addClass("active");
                $($(".text",$(this))).addClass("active");
            });
        }
    });
</script>


<div id="add_object_accommodation_iframe">
    <div class="full">
        <div class="title">Dodaj smještaj unutar objekta</div>
        {!! Form::open(['url' => URL::to('estate/addapartman'),'onsubmit' => 'return validateForm()']) !!}
            <table id="table_add_object" style="margin-top:40px;" class="table">
                <tr>
                    <td width="140"><a><label>Vrsta smještaja:<em>*</em></label></a></td>
                    <td width="400"><a>
                            <select style="width:206px;" class="input horizontal" name="apartman[basic][type]" id="vrsta" required="required" tabindex="1">
                                <option value="" selected="selected" id="open">&nbsp;</option>
                                <option value="Apartman">Apartman</option>
                                <option value="Soba">Soba</option>
                                <option value="Studio">Studio</option>
                                <option value="Kuća">Kuća</option>
                            </select>
                        </a></td>
                    <td></td>
                </tr>
                <tr>
                    <td><a><label>Pozicija:<em>*</em></label></a></td>
                    <td><a>
                            <select style="width:206px;" class="input horizontal" name="apartman[basic][level]" id="pozicija" required="required" tabindex="2">
                                <option value="" selected="selected" id="open">&nbsp;</option>
                                <option value="0">Prizemlje</option>
                                <option value="1">Kat</option>
                            </select>
                        </a></td>
                    <td></td>
                </tr>



                <tr>
                    <td><a><label>Osnovnih ležaja:<em>*</em></label></a></td>
                    <td><a><input class="input horizontal" style="width:200px;" name="apartman[basic][bed1]" id="lezaj" type="number" min="1" required tabindex="3" onKeyUp="cjenik_delay();"/></a></td>
                    <td></td>
                </tr>
                <tr>
                    <td><a><label>Pomoćnih ležaja:</label></a></td>
                    <td><a><input class="input horizontal" style="width:200px;" name="apartman[basic][bed2]" id="pomocni" type="number" min="0" tabindex="4" onKeyUp="cjenik_delay();"/></a></td>
                    <td></td>
                </tr>
                <tr>
                    <td><a><label>Broj spavaćih soba:<em>*</em></label></a></td>
                    <td><a><input class="input horizontal" style="width:200px;" name="apartman[basic][room]" type="room" tabindex="5"/></a></td>
                    <td></td>
                </tr>

                <tr>
                    <td><a style="height:40px;"></a></td>
                </tr>

                <tr>
                    <td><a style="margin-top:25px;"><label>Opremljenost:</label></a></td>
                    <td></td>
                    <td><a style="margin-top:25px;"><label>Ostale informacije:</label></a></td>
                </tr>

                <tr>
                    <td>
                        <div>
                            <input id="box_1" class="checkbox" name="apartman[details][tv]" type="checkbox" />
                            <label for="box_1" class="label big">TV</label>
                            <input id="box_2" class="checkbox" name="apartman[details][satellite]" type="checkbox" />
                            <label for="box_2" class="label big">Satelitska</label>
                            <input id="box_3" class="checkbox" name="apartman[details][dvd]" type="checkbox" />
                            <label for="box_3" class="label big">DVD</label>
                            <input id="box_4" class="checkbox" name="apartman[details][internet]" type="checkbox" />
                            <label for="box_4" class="label big">Internet</label>
                            <input id="box_5" class="checkbox" name="apartman[details][radio]" type="checkbox" />
                            <label for="box_5" class="label big">Radio</label>
                            <input id="box_6" class="checkbox" name="apartman[details][cooler]" type="checkbox" />
                            <label for="box_6" class="label big">Hladnjak</label>
                            <input id="box_7" class="checkbox" name="apartman[details][freezer]" type="checkbox" />
                            <label for="box_7" class="label big">Ledenica</label>
                            <input id="box_8" class="checkbox" name="apartman[details][stove]" type="checkbox" />
                            <label for="box_8" class="label big">Štednjak</label>
                            <input id="box_9" class="checkbox" name="apartman[details][stove2]" type="checkbox" />
                            <label for="box_9" class="label big">Kuhalo za vodu </label>
                            <input id="box_10" class="checkbox" name="apartman[details][barbecue]" type="checkbox" />
                            <label for="box_10" class="label big">Roštilj</label>
                            <input id="box_11" class="checkbox" name="apartman[details][aircondition]" type="checkbox" />
                            <label for="box_11" class="label big">Klima</label>
                            <input id="box_12" class="checkbox" name="apartman[details][washer]" type="checkbox" />
                            <label for="box_12" class="label big">Perilica</label>
                            <input id="box_13" class="checkbox" name="apartman[details][bathtub]" type="checkbox" />
                            <label for="box_13" class="label big">Kada</label>
                            <input id="box_14" class="checkbox" name="apartman[details][sea_view]" type="checkbox" />
                            <label for="box_14" class="label big">Tuš</label>
                            <input id="box_15" class="checkbox" name="apartman[details][towel]" type="checkbox" />
                            <label for="box_15" class="label big">Ručnici</label>
                        </div>
                    </td>
                    <td></td>
                    <td>
                        <div style="margin-right:-120px; padding-bottom:120px;">
                            <input id="box_16" class="checkbox" name="apartman[details][sea_view]" type="checkbox" />
                            <label for="box_16" class="label big">Pogled na more</label>
                            <input id="box_17" class="checkbox" name="apartman[details][parking]" type="checkbox" />
                            <label for="box_17" class="label big">Parking</label>
                            <input id="box_18" class="checkbox" name="apartman[details][garage]" type="checkbox" />
                            <label for="box_18" class="label big">Garaža</label>
                            <input id="box_19" class="checkbox" name="apartman[details][pool]" type="checkbox" />
                            <label for="box_19" class="label big">Bazen</label>
                            <input id="box_20" class="checkbox" name="apartman[details][terrace]" type="checkbox" />
                            <label for="box_20" class="label big">Terasa</label>
                            <input id="box_21" class="checkbox" name="apartman[details][breakfast]" type="checkbox" />
                            <label for="box_21" class="label big">Doručak</label>
                            <input id="box_22" class="checkbox" name="apartman[details][smoking]" type="checkbox" />
                            <label for="box_22" class="label big">Dozvoljeno pušenje</label>
                            <input id="box_23" class="checkbox" name="apartman[details][pet]" type="checkbox" />
                            <label for="box_23" class="label big">Držanje kućnih ljubimaca</label>
                            <input id="box_24" class="checkbox" name="apartman[details][disabled_person]" type="checkbox" />
                            <label for="box_24" class="label big">Prilagođeno osobama s invaliditetom</label>
                            <input id="box_25" class="checkbox" name="apartman[details][tax]" type="checkbox" />
                            <label for="box_25" class="label big">Boravišna taksa u cijeni</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><a style="margin-top:35px;"><label>Cjenik:<em>*</em></label></a></td>
                    <td></td>
                    <td>
                        <a style="margin-top:25px; margin-right:-200px;"><label>Usluge koje se dodatno naplaćuju:</label></a>
                        <textarea style="height:216px; max-height:216px; min-height:216px; max-width:360px; min-width:360px; width:360px; position:absolute; top:715px; left:545px;" class="input horizontal"  name="apartman[details][additional_price]" tabindex="26" placeholder="npr.                                        Parking: 2€/dan                        Klima: 1 €/dan                       Internet: 10 €/tjedan"></textarea>
                    </td>
                    <td></td>
                </tr>
                <input type="hidden" name="apartman[basic][real_estate_id]" value="<?php echo $estate;?>" />
                <tr id="cjenik_vrste">
                    <td>
                        <div style="background:#F00; width:500px; margin-right:-500px; margin-bottom:40px;">
                            <a style="margin-top:10px; float:left; margin-right:20px;">
                                <input id="cijena_noc" type="radio" name="apartman[basic][price_type]" checked="checked" onClick="cjenik(this)">
                                <label for="cijena_noc">Cijena po noćenju</label>
                            </a>
                            <a style="margin-top:10px; float:left; margin-right:20px;">
                                <input id="cijena_osoba" type="radio" name="apartman[basic][price_type]" onClick="cjenik(this)">
                                <label for="cijena_osoba">Cijena po osobi</label>
                            </a>
                            <a style="margin-top:10px; float:left; margin-right:20px;">
                                <input id="cijena_napredno" type="hidden" name="apartman[basic][price_type]" onClick="cjenik(this)">
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="cjenik" style="position:relative; height:177px; max-height:902px; width:450px; margin-right:-500px;">
                            <table id="tablica" style="margin-right:-500px; position:absolute; top:0px; left:0px;">
                                <input name="cjenik_vrsta" id="cjenik_vrsta" type="hidden" value="cijena_noc"/>
                                <tr>
                                    <td><div style="border:1px solid rgb(220, 225, 230); border-top:0px; line-height:32px; text-align:center; padding:2px 0px 2px 0px;">Od</div></td>
                                    <td><div style="border:1px solid rgb(220, 225, 230); border-top:0px; line-height:32px; text-align:center; padding:2px 0px 2px 0px;">Do</div></td>
                                    <td><div style="border:1px solid rgb(220, 225, 230); border-top:0px; height:32px; text-align:center; padding:2px 0px 2px 0px;">Minimalno<br/> dana boravka</div></td>
                                    <td><div style="border:1px solid rgb(220, 225, 230); border-top:0px; line-height:32px; text-align:center; padding:2px 0px 2px 0px;">Cijena u €</div></td>
                                </tr>

                                <?php
                                $br = 1;
                                $br_price = $min = 1;
                                $tab = 6;
                                $stranica = '';
                                for ($i=0; $i<5; $i++) {

                                    if ($i == 0){
                                        $stranica .= '
								  <tr>
									<td><input type="text" name="datum'.$br.'" value="" id="datum'.$br++.'" onMouseOver="new JsDatePick(this.id,{})" maxlength="20" class="input horizontal" style="width:50px;" required tabindex="'.$tab++.'">
									<td><input type="text" name="datum'.$br.'" value="" id="datum'.$br++.'" onMouseOver="new JsDatePick(this.id,{})" maxlength="20" class="input horizontal" style="width:50px;" required tabindex="'.$tab++.'">
									<td><input class="input horizontal" name="min'.$min.'" id="min'.$min++.'" type="number" min="0" style="width:90px;" tabindex="'.$tab++.'"/></td>
									<td><input class="input horizontal" name="price'.$br_price.'" id="price'.$br_price++.'" type="number" min="0" style="width:70px;" required tabindex="'.$tab++.'"/></td>
								  </tr>
		';

                                    }else{
                                        $stranica .= '
								  <tr>
									<td><input type="text" name="datum'.$br.'" value="" id="datum'.$br++.'" onMouseOver="new JsDatePick(this.id,{})" maxlength="20" class="input horizontal" style="width:50px;" tabindex="'.$tab++.'">
									<td><input type="text" name="datum'.$br.'" value="" id="datum'.$br++.'" onMouseOver="new JsDatePick(this.id,{})" maxlength="20" class="input horizontal" style="width:50px;" tabindex="'.$tab++.'">
									<td><input class="input horizontal" name="min'.$min.'" id="min'.$min++.'" type="number" min="0" style="width:90px;" tabindex="'.$tab++.'"/></td>
									<td><input class="input horizontal" name="price'.$br_price.'" id="price'.$br_price++.'" type="number" min="0" style="width:70px;" tabindex="'.$tab++.'"/></td>
								  </tr>
		';

                                    }


                                }


                                echo $stranica;

                                ?>

                            </table>
                        </div>
                    </td>
                    <td></td>
                    <td>
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <div class="button" onClick="dodaj_cijenu();" style="height:22px; line-height:22px; margin-right:-200px; position:relative; top:5px;">Dodaj Termin</div>
                    </td>
                </tr>
                <tr height="10"></tr>
                <tr>
                    <td>
                        <div style="margin-right:-1000px;">Ovdje upisujete cijenu koju Vi tražite.<br/> Povrh te cijene biti će obračunata minimalna naknada za troškove održavanja stranice.<br/> Na taj način biti će formirana konačna cijena koja neće biti jednaka vašoj traženoj.<br/> Ona može biti i niža ukoliko Vaša ponuda bude izabrana za sudjelovanje u jednoj od akcija koje ćemo provoditi.<br/> Bez obzira na cijenu koju formira stranica Vama će biti isplačen iznos koji ovdje zatražite.</div>
                    </td>
                </tr>
                <tr>
                    <td><a style="margin-top:30px;"><label>Dodatne informacije:</label></a></td>
                </tr>
                <tr>
                    <td><textarea style="max-height:220px; min-height:220px; height:220px; max-width:1092px; min-width:1092px; width:1092px; margin-right:-1092px;" class="input horizontal"  name="dodatno" tabindex="80"></textarea></td>
                </tr>
                <tr>
                    <td>
                        <div style="margin-right:-1000px; margin-bottom:15px;">U ovom prozoru upišite sve informacije o vašem smještaju koje nisu predviđene iznad, a smatrate ih relevantnima za svoju ponudu.<br/> Sve informacije koje navedete a upisane su iznad, bit će uklonjene zbog uštede prostora.</div>
                    </td>
                </tr>
                <tr>
                    <td><input class="button" type="submit" value="Dodaj" name="submit" /></td>
                    <td><a class="button" href="{{ URL::to('estate/addestate') }}" style="margin-left:-67px; height:28px; line-height:28px;">Odustani</a>
                        <input class="button" type="submit" value="Spremi i Završi" name="redirect" style="margin-left:10px;"></td>
                </tr>
            </table>
        </form>

    </div>

</div>


<div class="directions" style="top:3px; left:490px;">
    <div class="header">
        Upute za registraciju oglašivača:
    </div>
    <div class="text">
        <span>1.</span>	Ispunite svoje osobne podatke i odaberite Registriraj se
    </div>
    <div class="text">
        <span>2.</span>	Popunite podatke za Vaš objekt i odaberite Spremi na dnu stranice
    </div>
    <div class="text active">
        <span>3.</span> Ispunite informacije za pojedinačnu smještajnu jedinicu unutar objekta (za svaki apartman, sobu, studio apartman, kuću itd.) i odaberite Spremi
    </div>
    <div class="text">
        <span>4.</span>	Ponovite postupak iz točke 3. dok ne dodate sve pojedinačne jedinice unutar istog objekta
    </div>
    <div class="text">
        <span>5.</span>	Novi objekt dodajte pritiskom na rubriku Dodaj objekt
    </div>
</div>
</body>
</html>