@extends('common.content')

@section('js_before')
    <link rel="stylesheet" href="{{ URL::asset('style/slideshow.css') }}">
    <script src="{{ URL::asset('js/mootools-1.3.2-core.js') }}"></script>
    <script src="{{ URL::asset('js/mootools-1.3.2.1-more.js') }}"></script>
    <script src="{{ URL::asset('js/slideshow.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script type="text/javascript" src="{{ URL::asset('js/lokacija.js') }}"></script>
    <script>
        <?php 
        ////////////////////////////////
        // Ispis javascripta za slike //
        ////////////////////////////////
        //echo $slike;
        ?>
    </script>

    <script>
        function kalkulacija_top(){
        }
    </script>

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script type="text/javascript">

        $(function() {
            var objekt=<?php  echo $estate->id; ?>;
            var smjestaj=<?php  echo 0; ?>;
            bookingVani(objekt, smjestaj);
        });
    </script>
    <script type="text/javascript">
        function bookingVani(objekt, smjestaj){

//

        }
    </script>
@endsection

@section('js_after')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_map.css') }}" />
@endsection

@section('hide-show')
    <script type="text/javascript" src="{{ URL::asset('js/price.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/datepick_price.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {



            <!---------	 	ACCOMMODATION TABS     --------------->
            if ( $(".accommodation").length ) {
                    @foreach($estate->apartman as $apartman)
                    $(".acc{{ $apartman->id }}").click(function() {
                                $(".active").removeClass("active");
                                $(".acc{{ $apartman->id }}").addClass("active");
                            });
                    @endforeach
                    }

                    <!--------------	  READ MORE		------------------>
            $(".object_info .read.more").click(function() {
                $(".object_info").addClass("extend");
                $(".object_info .read.more").hide();
                $(".object_info .read.less").show();
            });
            $(".object_info .read.less").click(function() {
                $(".object_info").removeClass("extend");
                $(".object_info .read.less").hide();
                $(".object_info .read.more").show();
            });



            <!--------------	SEARCH BUTTON	------------------>
            $("#sidebar.your_search .button_green").click(function() {
                $("#sidebar.your_search").hide();
                $("#sidebar.new_search").show();
            });

            <!--------------  SIDEMAP POSITION	------------------>
            $(".map").mousedown(function() {
                $(".close_bg").fadeIn(350);
                $("#location").fadeIn(500);
                google.maps.event.trigger(map, 'resize');
                var latLng = new google.maps.LatLng({{ $estate->lat .','. $estate->long }});
                map.setCenter(latLng);
            });

            <!-------------------  CLOSE BG  --------------------->
            $(".close_bg").click(function() {
                $("#kalkulacija_top").hide();
                $("#location").hide();
                $(".close_bg").fadeOut(250);
            });

        });
    </script>

    <script>
        window.onload=function(){
            lokacija({{ $estate->lat }},{{ $estate->long }});
        };
    </script>


    <div class="close_bg"></div>
    <div id="kalkulacija_top">


    </div>

    <div id="location">
        <div class="location_map" id="lokacija"></div>
        <div class="information">
            <div class="information_text header">Adresa:</div>
            <div class="information_text">{{ $estate->address }}</div>
        </div>



        <div class="information">

            <?php  if($estate->details->distance_center || $estate->details->distance_beach || $estate->details->distance_store || $estate->details->distance_restoran || $estate->details->distance_disco || $estate->details->distance_transport || $estate->details->distance_ferry_city || $estate->details->distance_ferry || $estate->details->distance_plane_city || $estate->details->distance_plane) { ?>

            <div class="information_text header">Podaci o udaljenostima:</div>

            <?php  if($estate->details->distance_center){?>
            <div class="information_text">Centar mjesta: <span><?php  echo $estate->details->distance_center; ?></span></div>
            <?php  }if($estate->details->distance_beach){?>
            <div class="information_text">Najbliža plaža: <span><?php  echo $estate->details->distance_beach; ?></span></div>
            <?php  }if($estate->details->distance_store){?>
            <div class="information_text">Trgovina: <span><?php  echo $estate->details->distance_store; ?></span></div>
            <?php  }if($estate->details->distance_restoran){?>
            <div class="information_text">Restoran: <span><?php  echo $estate->details->distance_restoran; ?></span></div>
            <?php  }if($estate->details->distance_disco){?>
            <div class="information_text">Noćni klub: <span><?php  echo $estate->details->distance_disco; ?></span></div>
            <?php  }if($estate->details->distance_transport){?>
            <div class="information_text">Javni prijevoz: <span><?php  echo $estate->details->distance_transport; ?></span></div>
            <?php  }if($estate->details->distance_ferry_city){?>
            <div class="information_text">Najbliža trajektna luka: <span><?php  echo $estate->details->distance_ferry_city; ?></span></div>
            <?php  }if($estate->details->distance_ferry){?>
            <div class="information_text">Udaljenost od trajektne luke: <span><?php  echo $estate->details->distance_ferry; ?></span></div>
            <?php  }if($estate->details->distance_plane){?>
            <div class="information_text">Udaljenost od zračne luke: <span><?php  echo $estate->details->distance_plane; ?></span></div>
            <?php  } ?>
            <?php  } ?>

        </div>
    </div>
@endsection

@section('content')
    <div class="accommodation_full ">

        <div class="title"><?php  echo $estate->name; ?></div>

        <div class="path">
            <a>Hrvatska</a><span>>></span>
            <a><?php  echo ucwords($estate->city->region->name); ?></a><span>>></span>
            <a><?php  echo ucwords($estate->city->name); ?></a>
        </div>

        <div class="icons_wrap">
            <?php  if($estate->distance_center){?>
            <div class="icon center" title="Udaljenost do centra">
                <div class="text"><?php  echo $estate->details->distance_center; ?></div>
            </div>
            <?php }?>

            <?php  if($estate->distance_beach){?>
            <div class="icon beach" title="Udaljenost do plaže">
                <div class="text"><?php  echo $estate->details->distance_beach; ?></div>
            </div>
            <?php }?>
        </div>

        <div class="photos">
            <div id="overlap_1" class="slideshow">
                <img src="" alt="1">
            </div>
        </div>



        <div class="information">

            <div class="information_text header">Lokacija:</div>
            <div class="information_text">Adresa: <span><?php  echo $estate->address; ?></span></div>

            <div class="sidemap_hide">
                <div id="sidemap" class="map"></div>
            </div>





            <?php  if($estate->details->car){?>
            <div class="information_text">Moguć pristup automobilom</div>
            <?php  }if($estate->details->transport){?>
            <div class="information_text">Povezanost s javnim prijevozom</div>
            <?php  }if($estate->details->main_road){?>
            <div class="information_text">Pokraj glavne ceste</div>
            <?php  }if($estate->details->road_beach){?>
            <div class="information_text">Između objekta i plaže je glavna cesta </div>
            <?php  }if($estate->details->near_beach){?>
            <div class="information_text">Pokraj plaže</div>
            <?php  }if($estate->details->quiet){?>
            <div class="information_text">Relativno tiho okruženje</div>
            <?php  }if($estate->details->alone){?>
            <div class="information_text">Osama</div>
            <?php  }if($estate->details->green){?>
            <div class="information_text">Okružen zelenilom</div>

        <?php  if($estate->description) {?>
        <div class="object_info">
            <p>
                <?php  echo $estate->description; ?>
            </p>
        </div>
        <?php  }; ?>


</div>

                <!-----------------------	SMJESTAJ   ------------------------>
        <div class="booking_out" id="bookingVani">
            <div class="booking">
                <div id="printCijena">
                    <div class="price_top">već od</div>
                    <div class="price"><span>22 €</span></div>
                    <div class="price_bottom">po noćenju</div>
                </div>


                <?php if($estate->user->phone){ ?>

                <div class="booking_in_text header">Kontaktirajte vlasnika:</div>
                <div class="contact_text">
                    <div class="icon contact_phone"></div>
                    <div class="text header">Telefon:</div>
                    <div class="text"><?php echo $estate->user->phone; ?></div>
                </div>
                <?php } ?>



                <div class="booking_in_text header">Pošaljite upit vlasniku:</div>
                <div class="booking_in_text">Smještajna jedinica:</div>
                <div class="booking_in">
                    <div class="styled-select search_accommodation">
                        <select id="smjestajProfile" class="input search_accommodation select" style="color:#666666;" onchange="bookingVani(<?php echo $estate->id.',0'; ?>);">

                            @foreach($estate->apartman as $apartman)
                                <option value="{{ $apartman->id }}">{{ $apartman->type }}</option>';
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="booking_in_text">Termin rezervacije:</div>
                <div class="booking_in">
                    <input type="text" placeholder="Dolazak" id="dateProfileDolazak" onMouseOver="new JsDatePick(this.id,{})" onkeyup="bookingVani(<?php echo $estate->id.',0'; ?>);" class="input sidebar_in calendar search_accommodation" value="">
                </div>
                <div class="booking_in">
                    <input type="text" placeholder="Odlazak" id="dateProfileOdlazak" onMouseOver="new JsDatePick(this.id,{})" onkeyup="bookingVani(<?php echo $estate->id.',0'; ?>);" class="input sidebar_in calendar search_accommodation" value="">
                </div>

                <div class="booking_in_text">Broj osoba:</div>
                <div class="booking_in">
                    <div class="styled-select search_accommodation">
                        <select id="odraslaOsobaProfile" class="input search_accommodation select" style="color:#666666;" onchange="bookingVani(<?php echo $estate->id.',0'; ?>);">
                            <option value="" selected="selected">Odraslih (18+)</option>

                            <?php
                            for($j=1; $j<=5; $j++){
                                if($j == 1){
                                    echo '
                                <option value="'.$j.'">'.$j.' odrasla osoba</option>';
                                } elseif ($j > 1 AND $j < 5){
                                    echo '
                                <option value="'.$j.'">'.$j.' odrasle osobe</option>';
                                } else {
                                    echo '
                                <option value="'.$j.'">'.$j.' odraslih osoba</option>';
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>
                <div class="booking_in">
                    <div class="styled-select search_accommodation">
                        <select id="teenOsobaProfile" class="input search_accommodation select" style="color:#666666;" onchange="bookingVani(<?php echo $estate->id.',0'; ?>);">
                            <option value="" selected="selected">Tinejđera (12-18)</option>

                            <?php
                            for($j=1; $j<=4; $j++){
                                if($j == 1){
                                    echo '
                                <option value="'.$j.'" >'.$j.' tinejđer</option>';
                                } else {
                                    echo '
                                <option value="'.$j.'" >'.$j.' tinejđera</option>';
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>
                <div class="booking_in">
                    <div class="styled-select search_accommodation">
                        <select id="dijeteOsobaProfile" class="input search_accommodation select" style="color:#666666;" onchange="bookingVani(<?php echo $estate->id.',0'; ?>);">
                            <option value="" selected="selected">Djece (3-11)</option>

                            <?php
                            for($j=1; $j<=4-1; $j++){
                                if($j == 1){
                                    echo '
                                <option value="'.$j.'" >'.$j.' djete</option>';
                                } else {
                                    echo '
                                <option value="'.$j.'" >'.$j.' djece</option>';
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>
                <div class="booking_in">
                    <div class="styled-select search_accommodation">
                        <select id="bebaOsobaProfile" class="input search_accommodation select" style="color:#666666;" onchange="bookingVani(<?php echo $estate->id.',0'; ?>);">
                            <option value="" selected="selected">Beba (0-2)</option>

                            <?php
                            for($j=1; $j<=4-1; $j++){
                                if($j == 1){
                                    echo '
                                <option value="'.$j.'">'.$j.' beba</option>';
                                } elseif ($j > 1 AND $j < 5){
                                    echo '
                                <option value="'.$j.'">'.$j.' bebe</option>';
                                } else {
                                    echo '
                                <option value="'.$j.'">'.$j.' bebi</option>';
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>
            </div>

        </div>




        <div class="accommodation" style="float:left">
            @foreach($estate->apartman as $apartman)
                <div class="information_menu_tab acc{{ $apartman->id }} active">
                <span>{{ $apartman->type }}</span> ({{ $apartman->bed }})
                <div class="hide_shadow"></div>
                </div>
            @endforeach

                    <!-----------------------	SMJESTAJ   ------------------------>

            @foreach($estate->apartman as $apartman)
                    <div class="information_menu acc{{ $apartman->id }} active">
                        <div class="information_menu_left">


                            <div class="header main">{{ $apartman->type }}</div>
                            <div class="column">
                                <ul class="main">
                                    <li>
                                        Pozicija: <span>{{ $apartman->level ? 'Kat' : 'Prizemlje' }}</span>
                                    </li>
                                </ul>
                                <ul class="main">
                                    <li>
                                        Broj spavaćih soba: <span>{{ $apartman->room }}</span>
                                    </li>
                                </ul>
                                <ul class="main">
                                    <li>
                                        Broj osnovnih ležaja: <span>{{ $apartman->getBed1() }}</span>
                                    </li>
                                </ul>
                                <ul class="main">
                                    <?php if( $apartman->getBed2() ){ ?>
                                    <li>
                                        Broj pomoćnih ležaja: <span>{{ $apartman->getBed2() }}</span>
                                    </li>
                                    <?php }; ?>
                                </ul>
                            </div>

                            <div class="header">Opremljenost:</div>
                            <div class="column">
                                <div class="text">
                                    <ul>
                                        <li id="<?php echo $apartman->details->tv ? "true" : "false"; ?>" class="short">TV</li>
                                        <li id="<?php echo $apartman->details->satellite ? "true" : "false"; ?>" class="short">Satelitska</li>
                                        <li id="<?php echo $apartman->details->aircondition ? "true" : "false"; ?>" class="short">Klima</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="column">
                                <div class="text">
                                    <ul>
                                        <li id="<?php echo $apartman->details->cooler ? "true" : "false"; ?>" class="short">Hladnjak</li>
                                        <li id="<?php echo $apartman->details->stove ? "true" : "false"; ?>" class="short">Štednjak</li>
                                        <li id="<?php echo$apartman->details->barbecue ? "true" : "false"; ?>" class="short">Roštilj</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="column">
                                <div class="text">
                                    <ul>
                                        <li id="<?php echo $apartman->details->washer ? "true" : "false"; ?>" class="short">Perilica</li>
                                        <li id="<?php echo $apartman->details->bathtub ? "true" : "false"; ?>" class="short">Kada</li>
                                        <li id="<?php echo $apartman->details->shower ? "true" : "false"; ?>" class="short">Tuš</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="header">Ostale Informacije:</div>
                            <div class="column">
                                <div class="text">
                                    <ul>
                                        <li id="<?php echo $apartman->details->parking ? "true" : "false"; ?>" class="short">Parking</li>
                                        <li id="<?php echo $apartman->details->garage ? "true" : "false"; ?>" class="short">Garaža</li>
                                        <li id="<?php echo $apartman->details->pool ? "true" : "false"; ?>" class="short">Bazen</li>
                                        <li id="<?php echo $apartman->details->terrace ? "true" : "false"; ?>" class="short">Terasa</li>
                                    </ul>
                                </div>
                            </div>


                            <div class="column">
                                <div class="text">
                                    <ul>
                                        <li id="<?php echo $apartman->details->sea_view ? "true" : "false"; ?>" class="short">Pogled na more</li>
                                        <li id="<?php echo $apartman->details->breakfast ? "true" : "false"; ?>" class="short">Doručak</li>
                                        <li id="<?php echo $apartman->details->pet ? "true" : "false"; ?>" class="short">Držanje kućnih ljubimaca</li>
                                        <li id="<?php echo $apartman->details->disabled_person ? "true" : "false"; ?>" class="short">Pristup kolicima</li>
                                    </ul>
                                </div>
                            </div>

                    <div class="header">Cjenik:</div>
                    <div class="column">
                        <div class="text">

                            <ul style="padding-bottom:5px;">
                                <li class="huge">
                                    <div class="header">


                                        <?php 


                                        $price = $apartman->price;
                                        $price = explode(";", $price);

                                        for ($k=0; $k<count($price); $k++){
                                            $price[$k] = ceil( $price[$k]+($estate->provision/100*$price[$k]) );
                                        }

                                            $kapacitet = explode('+', $apartman->bed);
                                        $kapacitet = $kapacitet[0] + $kapacitet[1];


                                        switch (true){
                                            case ($kapacitet>=3):
                                                $br_osoba = 3;
                                                $margin = 'style="margin-left:0px;"';
                                                break;
                                            case ($kapacitet==2):
                                                $br_osoba = 2;
                                                $margin = 'style="margin-left:30px;"';
                                                break;
                                            case ($kapacitet==1):
                                                $br_osoba = 1;
                                                $margin = 'style="margin-left:61px;"';
                                                break;
                                        }
                                        ?>


                                        <div id="price_list">
                                            <div class="left top">
                                                <div style="margin-top:23px;"><span>Datum</span></div>
                                            </div>
                                            <div class="center top">
                                                <div style="margin-top:23px;"><span>Min. dana</span></div>
                                            </div>
                                            <div class="right top">
                                            <span>
                                                <div>Broj osoba</div>
                                                <?php  for($m=2, $l=0; $m>=0; $m--){
                                                if (($kapacitet-$m)>0){ ?>
                                                <div class="nop" <?php  echo !$l ? $margin : ""; ?>><?php  echo ($kapacitet-$m); ?></div>
                                                <?php $l++;
                                                }} ?>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="price_list">
                                        <?php 
                                        $dates = explode(";",$estate->dates);
                                        $min = explode(";",$estate->minimum_people);
                                        $osoba = explode(";",$estate->people);
                                        $cjenik_vrsta = $estate->price_type;

                                        for ($j=0, $k=0; $j<count($dates); $j++){
                                        ?>


                                        <div class="left"><?php  echo $dates[$j]; ?></div>
                                        <div class="center"><?php  echo $min[$j]; ?></div>
                                        <div class="right">
                                            <?php 
                                            if($cjenik_vrsta == 'cijena_napredno'){

                                            for($m=0, $l=0; $m<$br_osoba; $m++, $k++){
                                            ?> <div class="nop" <?php  echo !$l ? $margin : ""; ?>><?php  echo $price[$k]; ?> €</div><?php 
                                            $l++;
                                            }

                                            }elseif($cjenik_vrsta == 'cijena_osoba'){

                                            for($m=0, $l=0; $m<$br_osoba; $m++, $k++){

                                            if($osoba[$j] > (($kapacitet-$br_osoba)+$m+1)){
                                                $cijena = $price[$j]*$osoba[$j];
                                            }else{
                                                $cijena = $price[$j]*(($kapacitet-$br_osoba)+$m+1);
                                            }

                                            ?> <div class="nop" <?php  echo !$l ? $margin : ""; ?>><?php  echo $cijena; ?> €</div><?php 
                                            $l++;
                                            }


                                            }else{

                                            for($m=0, $l=0; $m<$br_osoba; $m++, $k++){

                                            ?> <div class="nop" <?php  echo !$l ? $margin : ""; ?>><?php  echo $price[$j]; ?> €</div> <?php 
                                            $l++;
                                            }

                                            }

                                            ?>

                                        </div>
                                        <?php  } ?>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>

                    <div class="column" style="display:none;">
                        <div class="text">
                            <ul>
                                <li class="long">
                                    <div class="header"><span>Naknada otkazivanja:</span></div>
                                </li>
                                <li><a style="float:left; margin-left:3px; line-height:18px;">do 29 dana</a><a style="float:right; margin-right:3px;">30 % ukupne cijene</a></li>
                                <li><a style="float:left; margin-left:3px; line-height:18px;">od 28 do 22 dana</a><a style="float:right; margin-right:3px;">40 % ukupne cijene</a></li>
                                <li><a style="float:left; margin-left:3px; line-height:18px;">od 21 do 15 dana</a><a style="float:right; margin-right:3px;">60 % ukupne cijene</a></li>
                                <li><a style="float:left; margin-left:3px; line-height:18px;">od 14 do 8 dana</a><a style="float:right; margin-right:3px;">80 % ukupne cijene</a></li>
                                <li><a style="float:left; margin-left:3px; line-height:18px;">od 7 do 0 dana</a><a style="float:right; margin-right:3px;">100 % ukupne cijene</a></li>
                            </ul>
                        </div>
                    </div>




                    <?php  if($estate->details->additional_price) {?>
                    <div class="column" style="float:right; display:none;">
                        <div class="text">
                            <ul>
                                <li class="long">
                                    <div class="header"><span>Nadoplata:</span></div>
                                </li>
                                <li class="long">
                                <pre>
                                    <div class="pre"><?php  echo $estate->details->additional_price; ?></div>
                                </pre>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php  } ?>

                </div>
                        @endforeach



                <div class="contact">
                    <div class="contact_text header">Kontakt</div>
                    <div class="contact_text">
                        Imate pitanja ili probleme sa rezervacijom?
                        Kontaktirajte nas!
                    </div>
                    <div class="contact_text">
                        <div class="icon contact_clock"></div>
                        <div class="text header">korisnička podrška:</div>
                        <div class="text">Pon-Sub 13:00-19:00</div>
                    </div>
                    <div class="contact_text">
                        <div class="icon contact_mail"></div>
                        <div class="text header">email:</div>
                        <div class="text">info@smjestajhrvatska.com</div>
                    </div>
                </div>



            </div>

            <?php  } ?>
        </div>
    </div>

@endsection