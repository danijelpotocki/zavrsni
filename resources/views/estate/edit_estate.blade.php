<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Smještaj Hrvatska | Privatni Smještaj | Apartmani, Sobe, Studio Apartmani i Kuće</title>


    <!-- STYLE CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_structure.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_nav_footer.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_map.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_content.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_objects.css') }}" />

    <!-- FUNCTIONS -->
    <script type="text/javascript" src="{{ URL::asset('js/if_gmap.js') }}"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var elements = document.getElementsByTagName("INPUT");
            for (var i = 0; i < elements.length; i++) {
                elements[i].oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        e.target.setCustomValidity("Polja označena zvijezdicom su obavezna.");
                    }
                };
                elements[i].oninput = function(e) {
                    e.target.setCustomValidity("");
                };
            }

            var selements = document.getElementsByTagName("SELECT");
            for (var i = 0; i < selements.length; i++) {
                selements[i].oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        e.target.setCustomValidity("Polja označena zvijezdicom su obavezna.");
                    }
                };
                selements[i].onchange = function(e) {
                    e.target.setCustomValidity("");
                };
            }

            $('#mjesto').on('change', function() {
                mapCenter( this.value );
            });
        });

        $(document).unbind('keydown').bind('keydown', function (event) {
            var doPrevent = false;
            if (event.keyCode === 8) {
                var d = event.srcElement || event.target;
                if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD'))
                        || d.tagName.toUpperCase() === 'TEXTAREA') {
                    doPrevent = d.readOnly || d.disabled;
                }
                else {
                    doPrevent = true;
                }
            }

            if (doPrevent) {
                event.preventDefault();
            }
        });
    </script>
    <script>
        function validateForm(){
            var vrsta = document.forms["podaci"]["vrsta"].value;
            var ime	= document.forms["podaci"]["ime"].value;
            var pbroj = document.forms["podaci"]["pbroj"].value;
            var adresa = document.forms["podaci"]["adresa"].value;
            var mjesto = document.forms["podaci"]["mjesto"].value;
            var longval = document.forms["podaci"]["longval"].value;
            var latval = document.forms["podaci"]["latval"].value;
            var uvjet = document.forms["podaci"]["uvjet"].checked;

            if ((vrsta==null || vrsta=="") || (ime==null || ime=="") || (pbroj==null || pbroj=="") || (adresa==null || adresa=="") || (mjesto==null || mjesto=="")){
                alert("Molimo ispunite sva polja označena zvjezdicom.");
                return false;
            }

            if(longval=="16.430054" || longval==null || longval=="" || latval=="44.621754" || latval==null || latval==""){
                alert("Molimo označite svoj objekt na karti.");
                return false;
            }

            if(!uvjet){
                alert("Ako želite oglašavati svoj objekt morate se slagati sa općim uvjetima.");
                return false;
            }
        }
    </script>
</head>



<body style="float:left; margin:0px; width:1098px; background:none; position:relative;" onLoad="if_gmap_init();">


<div id="add_object_accommodation_iframe">
    <div class="full">
        <div class="title">Dodaj objekt</div>
        {!! Form::open(['onsubmit' => 'return validateForm()']) !!}
            <table id="table_add_object" class="table">
                <tr height="20"></tr>
                <tr>
                    <td width="200"><a><label>Ime objekta:<em>*</em></label></a></td>
                    <td width="350"><input style="width:194px;" class="input horizontal" name="apartman[basic][name]" type="text" tabindex="2" required value="{{ $estate->name }}"/></td>
                    <td width="200"></td>
                    <td></td>
                </tr>

                @if($estate->id)
                    <input type="hidden" name="estate_id" value="{{ $estate->id }}">
                @endif
                <tr>
                    <td><a style="margin-top:60px;"><label>Sadrži:</label></a></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <div style="margin-right:-150px;">

                            <input id="box_1" class="checkbox" name="apartman[details][car]" type="checkbox" {{ isset($estate->details->car) && $estate->details->car ? 'checked' : '' }}>
                            <label for="box_1" class="label big">Moguć pristup automobilom</label>

                            <input id="box_2" class="checkbox" name="apartman[details][transport]" type="checkbox" {{ isset($estate->details->transport) && $estate->details->transport ? 'checked' : '' }}>
                            <label for="box_2" class="label big">Povezanost s javnim prijevozom</label>

                            <input id="box_3" class="checkbox" name="apartman[details][main_road]" type="checkbox" {{ isset($estate->details->main_road) && $estate->details->main_road ? 'checked' : '' }}>
                            <label for="box_3" class="label big">Pokraj glavne ceste</label>

                            <input id="box_4" class="checkbox" name="apartman[details][road_beach]" type="checkbox" {{ isset($estate->details->road_beach) && $estate->details->road_beach ? 'checked' : '' }}>
                            <label for="box_4" class="label big">Između objekta i plaže je glavna cesta</label>

                            <input id="box_5" class="checkbox" name="apartman[details][near_beach]" type="checkbox" {{ isset($estate->details->near_beach) && $estate->details->near_beach ? 'checked' : '' }}>
                            <label for="box_5" class="label big">Pokraj plaže</label>

                            <input id="box_6" class="checkbox" name="apartman[details][quiet]" type="checkbox" {{ isset($estate->details->quiet) && $estate->details->quiet ? 'checked' : '' }}>
                            <label for="box_6" class="label big">Relativno tiho okruženje</label>

                        </div>
                    </td>
                    <td></td>
                    <td>
                        <div style="margin-left:-120px;">

                            <input id="box_7" class="checkbox" name="apartman[details][alone]" type="checkbox" {{ isset($estate->details->alone) && $estate->details->alone ? 'checked' : '' }}>
                            <label for="box_7" class="label big">Osama</label>

                            <input id="box_8" class="checkbox" name="apartman[details][green]" type="checkbox" {{ isset($estate->details->green) && $estate->details->green ? 'checked' : '' }}>
                            <label for="box_8" class="label big">Okružen zelenilom</label>

                            <input id="box_9" class="checkbox" name="apartman[details][pebble]" type="checkbox" {{ isset($estate->details->pebble) && $estate->details->pebble ? 'checked' : '' }}>
                            <label for="box_9" class="label big">Šljunčana plaža</label>

                            <input id="box_10" class="checkbox" name="apartman[details][sand]" type="checkbox" {{ isset($estate->details->sand) && $estate->details->sand ? 'checked' : '' }}>
                            <label for="box_10" class="label big">Pješčana plaža</label>

                            <input id="box_11" class="checkbox" name="apartman[details][concrete]" type="checkbox" {{ isset($estate->details->concrete) && $estate->details->concrete ? 'checked' : '' }}>
                            <label for="box_11" class="label big">Betonska plaža</label>

                            <input id="box_12" class="checkbox" name="apartman[details][rock]" type="checkbox" {{ isset($estate->details->rock) && $estate->details->rock ? 'checked' : '' }}>
                            <label for="box_12" class="label big">Stjenovita plaža</label>

                        </div>
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td><a style="height:60px;"></a></td>
                </tr>

                <tr>
                    <td><a><label>Poštanski broj:<em>*</em></label></a></td>
                    <td><a><input style="width:194px; margin-left:-70px;" class="input horizontal" name="apartman[basic][postal_code]" type="number" tabindex="6" required value="{{ $estate->postal_code }}"/></a></td>
                </tr>
                <tr>
                    <td><a><label>Adresa:<em>*</em></label></a></td>
                    <td><input style="width:194px; margin-left:-70px;" class="input horizontal" name="apartman[basic][address]" type="text" tabindex="7" required value="{{ $estate->address }}"/></td>
                </tr>
                <tr>
                    <td><a><label>Mjesto:<em>*</em></label></a></td>
                    <td><a><select  style="width:200px; margin-left:-70px;" class="input horizontal" name="apartman[basic][city_id]" id="mjesto" tabindex="5" required="required">
                                <option selected="selected" disabled="true">-- Odaberite mjesto --</option>';
                                @foreach($cities as $city)
                                    <option value="{{ $city->id.','.$city->lat .','.$city->long .','.$city->zoom_level }}">{{ $city->name }}</option>';
                                @endforeach
                            </select></a>
                                <a style="margin:7px -400px 0px 10px;"><label>Odaberite mjesto najbliže Vašem.</label></a>
                    </td>
                </tr>
                <tr>
                    <td><a style="margin-top:5px; margin-right:-100px;"><label>Precizno označite objekt na karti:<em>*</em></label></a></td>
                </tr>
                <tr>
                    <td id="maparea">
                        <input type="hidden" name="longval" id="apartman[basic][long]" required="required" value="{{ $estate->long }}">
                        <input type="hidden" name="latval" id="apartman[basic][lat]" required="required" value="{{ $estate->lat }}">
                        <div id="mapitems" style="width:1098px; height:550px; margin-right:-1098px;"></div>
                    </td>
                </tr>
                <tr>
                    <td><a style="margin-top:20px;"><label>Centar mjesta:</label></a></td>
                    <td><a style="margin-top:20px;">
                            <select class="input horizontal" name="apartman[details][distance_center]" id="centar" style="width:200px;" tabindex="8">
                                <option selected="selected" value="0">Nije označena udaljenost</option>
                                <option value="50 m"> 50 m </option>
                                <option value="100 m"> 100 m </option>
                                <option value="200 m"> 200 m </option>
                                <option value="500 m"> 500 m </option>
                                <option value="1 km"> 1 km </option>
                                <option value="3 km"> 3 km </option>
                                <option value="5 km"> 5 km </option>
                                <option value="10 km"> 10 km </option>
                                <option value="20 km"> 20 km </option>
                                <option value="50 km"> 50 km </option>
                            </select>
                        </a></td>
                    <td><a style="margin-top:20px;"><label>Najbliža plaža:</label></a></td>
                    <td><a style="margin-top:20px;">
                            <select class="input horizontal" name="apartman[details][distance_beach]" id="plaza" style="width:200px;" tabindex="13">
                                <option selected="selected" value="0">Nije označena udaljenost</option>
                                <option value="10 m"> 10 m </option>
                                <option value="100 m"> 100 m </option>
                                <option value="200 m"> 200 m </option>
                                <option value="500 m"> 500 m </option>
                                <option value="1 km"> 1 km </option>
                                <option value="3 km"> 3 km </option>
                                <option value="5 km"> 5 km </option>
                                <option value="10 km"> 10 km </option>
                                <option value="20 km"> 20 km </option>
                                <option value="50 km"> 50 km </option>
                            </select>
                        </a></td>
                </tr>
                <tr>
                    <td><a><label>Trgovina:</label></a></td>
                    <td><a>
                            <select class="input horizontal" name="apartman[details][distance_store]" id="trgovina" style="width:200px;" tabindex="9">
                                <option selected="selected" value="0">Nije označena udaljenost</option>
                                <option value="50 m"> 50 m </option>
                                <option value="100 m"> 100 m </option>
                                <option value="200 m"> 200 m </option>
                                <option value="500 m"> 500 m </option>
                                <option value="1 km"> 1 km </option>
                                <option value="3 km"> 3 km </option>
                                <option value="5 km"> 5 km </option>
                                <option value="10 km"> 10 km </option>
                                <option value="20 km"> 20 km </option>
                                <option value="50 km"> 50 km </option>
                            </select>
                        </a></td>
                    <td><a><label>Restoran:</label></a></td>
                    <td><a>
                            <select class="input horizontal" name="apartman[details][distance_restoran]" id="restoran" style="width:200px;" tabindex="14">
                                <option selected="selected" value="0">Nije označena udaljenost</option>
                                <option value="50 m"> 50 m </option>
                                <option value="100 m"> 100 m </option>
                                <option value="200 m"> 200 m </option>
                                <option value="500 m"> 500 m </option>
                                <option value="1 km"> 1 km </option>
                                <option value="3 km"> 3 km </option>
                                <option value="5 km"> 5 km </option>
                                <option value="10 km"> 10 km </option>
                                <option value="20 km"> 20 km </option>
                                <option value="50 km"> 50 km </option>
                            </select>
                        </a></td>
                </tr>
                <tr>
                    <td><a><label>Noćni klub:</label></a></td>
                    <td><a>
                            <select class="input horizontal" name="apartman[details][distance_disco]" id="disco" style="width:200px;" tabindex="10">
                                <option selected="selected" value="0">Nije označena udaljenost</option>
                                <option value="50 m"> 50 m </option>
                                <option value="100 m"> 100 m </option>
                                <option value="200 m"> 200 m </option>
                                <option value="500 m"> 500 m </option>
                                <option value="1 km"> 1 km </option>
                                <option value="3 km"> 3 km </option>
                                <option value="5 km"> 5 km </option>
                                <option value="10 km"> 10 km </option>
                                <option value="20 km"> 20 km </option>
                                <option value="50 km"> 50 km </option>
                            </select>
                        </a></td>
                    <td><a><label>Javni prijevoz:</label></a></td>
                    <td><a>
                            <select class="input horizontal" name="apartman[details][distance_transport]" id="prijevoz" style="width:200px;" tabindex="15">
                                <option selected="selected" value="0">Nije označena udaljenost</option>
                                <option value="50 m"> 50 m </option>
                                <option value="100 m"> 100 m </option>
                                <option value="200 m"> 200 m </option>
                                <option value="500 m"> 500 m </option>
                                <option value="1 km"> 1 km </option>
                                <option value="3 km"> 3 km </option>
                                <option value="5 km"> 5 km </option>
                                <option value="10 km"> 10 km </option>
                                <option value="20 km"> 20 km </option>
                                <option value="50 km"> 50 km </option>
                            </select>
                        </a></td>
                </tr>



                <tr>
                    <td><a><label>Najbliža trajektna luka:</label></a></td>
                    <td><a>
                            <select class="input horizontal" name="apartman[details][distance_ferry_city]" id="trajektgrad" style="width:200px;" tabindex="11">
                                <option selected="selected" value="0">Nije odabrana trajektna luka</option>
                                <option value="Dubrovnik"> Dubrovnik </option>
                                <option value="Drvenik"> Drvenik </option>
                                <option value="Pula"> Pula </option>
                                <option value="Rijeka"> Rijeka </option>
                                <option value="Split"> Split </option>
                                <option value="Stari Grad - Hvar"> Stari Grad - Hvar </option>
                                <option value="Supetar - Bol"> Supetar - Bol </option>
                                <option value="Vela Luka - Korčula"> Vela Luka - Korčula </option>
                                <option value="Zadar"> Zadar </option>
                            </select>
                        </a></td>
                    <td><a><label>Najbliža zračna luka:</label></a></td>
                    <td><a>
                            <select class="input horizontal" name="apartman[details][distance_plane_city]" id="aviongrad" style="width:200px;" tabindex="16">
                                <option selected="selected" value="0">Nije odabrana zračna luka</option>
                                <option value="Brač"> Brač </option>
                                <option value="Dubrovnik"> Dubrovnik </option>
                                <option value="Mali Lošinj"> Mali Lošinj </option>
                                <option value="Osijek"> Osijek </option>
                                <option value="Pula"> Pula </option>
                                <option value="Rijeka"> Rijeka </option>
                                <option value="Split"> Split </option>
                                <option value="Zadar"> Zadar </option>
                                <option value="Zagreb"> Zagreb </option>
                            </select>
                        </a></td>
                </tr>
                <tr>
                    <td><a><label>Udaljenost od trajektne luke:</label></a></td>
                    <td><a>
                            <select class="input horizontal" name="apartman[details][distance_ferry]" id="trajekt" style="width:200px;" tabindex="12">
                                <option selected="selected" value="0">Nije označena udaljenost</option>
                                <option value="50 m"> 50 m </option>
                                <option value="100 m"> 100 m </option>
                                <option value="200 m"> 200 m </option>
                                <option value="500 m"> 500 m </option>
                                <option value="1 km"> 1 km </option>
                                <option value="3 km"> 3 km </option>
                                <option value="5 km"> 5 km </option>
                                <option value="10 km"> 10 km </option>
                                <option value="20 km"> 20 km </option>
                                <option value="50 km"> 50 km </option>
                            </select>
                        </a></td>
                    <td><a><label>Udaljenost od zračne luke:</label></a></td>
                    <td><a>
                            <select class="input horizontal" name="apartman[details][distance_plane]" id="avion" style="width:200px;" tabindex="17">
                                <option selected="selected" value="0">Nije označena udaljenost</option>
                                <option value="50 m"> 50 m </option>
                                <option value="100 m"> 100 m </option>
                                <option value="200 m"> 200 m </option>
                                <option value="500 m"> 500 m </option>
                                <option value="1 km"> 1 km </option>
                                <option value="3 km"> 3 km </option>
                                <option value="5 km"> 5 km </option>
                                <option value="10 km"> 10 km </option>
                                <option value="20 km"> 20 km </option>
                                <option value="50 km"> 50 km </option>
                            </select>
                        </a></td>
                </tr>
                <tr>
                    <td><a style="height:30px;"></a></td>
                </tr>
                <tr>
                    <td><a><label>Dodatne informacije:</label></a></td>
                </tr>
                <tr>
                    <td><textarea style="max-height:85px; min-height:85px; height:85px; max-width:1092px; min-width:1092px; width:1092px; margin-right:-1092px;" class="input horizontal"  name="apartman[basic][description]" tabindex="18"></textarea></td>
                </tr>
                <tr>
                    <td>
                        <div style="margin-right:-1000px; margin-bottom:13px;">
                            U ovom prozoru upišite sve informacije o vašem smještaju koje nisu predviđene iznad, a smatrate ih relevantnima za svoju ponudu.<br/>
                            Sve informacije koje navedete a upisane su iznad, bit će uklonjene zbog uštede prostora.
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="margin-right:-1000px; margin-top:20px; margin-bottom:15px;">
                            <input id="box_13" class="checkbox" type="checkbox" name="uvjet">
                            <label class="label big" for="box_13">
                                Potvrđujem da sam upoznat i slažem se s <a href="" style="float:none; color:#64c558;" target="_blank">Općim uvjetima za iznajmljivače</a>.
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><input class="button" type="submit" value="Dodaj" name="submit"/></td>
                    <td><a class="button" href="{{ URL::to('estate/addestate') }}" style="margin-left:-125px; height:28px; line-height:28px;">Odustani</a></td>
                </tr>
            </table>
        {!! Form::close() !!}

    </div>
</div>



<div class="directions" style="top:3px; left:490px;">
    <div class="header">
        Upute za registraciju oglašivača:
    </div>
    <div class="text">
        <span>1.</span>	Ispunite svoje osobne podatke i odaberite Registriraj se
    </div>
    <div class="text active">
        <span>2.</span>	Popunite podatke za Vaš objekt i odaberite Spremi na dnu stranice
    </div>
    <div class="text">
        <span>3.</span> Ispunite informacije za pojedinačnu smještajnu jedinicu unutar objekta (za svaki apartman, sobu, studio apartman, kuću itd.) i odaberite Spremi
    </div>
    <div class="text">
        <span>4.</span>	Ponovite postupak iz točke 3. dok ne dodate sve pojedinačne jedinice unutar istog objekta
    </div>
    <div class="text">
        <span>5.</span>	Novi objekt dodajte pritiskom na rubriku Dodaj objekt
    </div>
</div>

</body>
</html>