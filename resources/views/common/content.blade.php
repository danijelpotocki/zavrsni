<!DOCTYPE html>
<html>
<!-- InstanceBegin template="/Templates/index.dwt" codeOutsideHTMLIsLocked="false" -->
<head>


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="Language" content="hr"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('images/favicon.ico') }}">

    <!-- DEFINE -->
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>Smještaj Hrvatska | Privatni Smještaj | Apartmani, Sobe, Studio Apartmani i Kuće</title>
    <!-- InstanceEndEditable -->
    <meta name="Keywords"
          content="apartmani, privatni, smještaj, smjestaj, rezervacija, zdrava, prirodna, prehrana, zadar, split, vir, pula, rovinj, trogir, rijeka, zagreb, novi, vinodolski, hvar, dubrovnik, makarska, kvarner, nocenje, noćenje, specijaliteti, maslinovo, ulje, kvalitetno, vino, iznajmljivanje, apartmana, med, janjetina, eko-otok, Silba, regije, Apartmani, rivijere, otoci, mjesta, ljetovališta, apartmani, za, iznajmljivanje, smještaja, apartmana, turizam, ljetovanje, sportovi na vodi, sobe, kuće, kampovi, zdrava, hrana, plivanje, jedrenje, ronjenje, surfanje, daskanje, Hrvatska, obala, Jadransko, more, Jadran, sunce, plaža, masline, kapari, vino, priroda, eko-turizam, zdrav, život, ekologija"/>
    <meta name="Description"
          content="Na najjednostavniji način rezervirajte svoj smještaj u Hrvatskoj. Potpuno besplatno oglašavajte svoju turističku ponudu."/>
    <meta name="Robots" content="index, follow">

    <!-- STYLE CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_structure.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_nav_footer.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('style/style_content.css') }}"/>

    <!-- InstanceBeginEditable name="JAVASCRIPT-CSS - BEFORE" -->

    @yield('js_before')

    <!-- InstanceEndEditable -->

    <!-- FUNCTIONS -->
    <script type="text/javascript" src="{{ URL::asset('js/jQuery.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/ajax.googleapi.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.cycle.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.nav.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/functions.js') }}"></script>

    <!-- InstanceBeginEditable name="JAVASCRIPT-CSS - AFTER" -->

    @yield('js_after')

    <!-- InstanceEndEditable -->

</head>
<body>
<div class="old_IE">
    <div class="text">
        Molimo Vas da unaprijedite Internet Explorer na najnoviju verziju.<br/>
        <a target="_blank" href="http://www.microsoft.com/en-us/download/internet-explorer-10-details.aspx">Najnoviju
            verziju možete pronaći ovdje..</a>
    </div>
</div>

<div class="top_nav"></div>

<div class="top_bg">
    <div class="border top"></div>
</div>


<div class="phone">+385 95 8854 556</div>
<div class="mail">info@smjestajhrvatska.com</div>
<div class="time">Pon-Sub 13:00-19:00</div>


<a id="logo" href="{{ URL::to('/') }}"></a>


<ul id="nav" role="navigation">
    <li class="main first">
        <a href="{{ URL::to('/map') }}?map=ap" class="mainlink">Apartmani</a>
        <a href="{{ URL::to('/map') }}?map=ap"
           class="mainlink number"><?php echo Helper::brojac_apartman(); ?></a>
    </li>
    <li class="main">
        <a href="{{ URL::to('/map') }}?map=so" class="mainlink">Sobe</a>
        <a href="{{ URL::to('/map') }}?map=so" class="mainlink number"><?php echo Helper:: brojac_soba(); ?></a>
    </li>
    <li class="main">
        <a href="{{ URL::to('/map') }}?map=st" class="mainlink">Studio apartmani</a>
        <a href="{{ URL::to('/map') }}?map=st"
           class="mainlink number"><?php echo Helper::brojac_studio(); ?></a>
    </li>
    <li class="main last">
        <a href="{{ URL::to('/map') }}?map=ku" class="mainlink">Kuće za odmor</a>
        <a href="{{ URL::to('/map') }}?map=ku" class="mainlink number"><?php echo Helper::brojac_kuca(); ?></a>
    </li>
</ul>


<!----------------------------------   LOGIN/REGISTER   ---------------------------------->
@if(Auth::check())
    <div id="user_dropdown">
        <div class="current">{{ Auth::user()->name . ' ' . Auth::user()->surname }}</div>
        <div class="nav_arrowdown_click"></div>
        <div class="dropdown">
            <a href="{{ URL::to('/account') }}" class="user">Profil</a>
            <a href="{{ URL::to('/account') }}?page=mo" class="user">Moji Objekti</a>
            <a href="{{ URL::to('/account') }}?page=do" class="user">Dodaj Objekt</a>
            <a href="{{ URL::to('/account/logout') }}" class="user">Odjavi se</a>
        </div>
    </div>
@else
    <div id="login_register_dropdown">
        <div class="current">Prijava | Registracija</div>
        <div class="nav_arrowdown_click"></div>

        <div class="dropdown">

                {!! Form::open(['url' => URL::to('/account/login')]) !!}
                <table width="270" border="0" style="margin-bottom:13px;">
                    <tr>
                        <td width="100">Vaš E-mail:</td>
                        <td><input class="input_nav" id="email" name="email" required="required" type="text" size="20"/></td>
                    </tr>
                    <tr>
                        <td width="170">Vaša Lozinka:</td>
                        <td><input class="input_nav" id="password" name="password" required="required" type="password" size="20"/></td>
                    </tr>
                    <tr>
                        <td><input class="button_nav" type="submit" value="Prijavi se" name="submit" /></td>
                        <td><a href="{{ URL::to('/account/forgotten') }}">Zaboravili ste lozinku?</a></td>
                    </tr>
                </table>
            {!! Form::close() !!}


            <div class="login_register_border"></div>
            <p>Besplatno registrirajte svoj objekt</p>
            <form action="{{ URL::to('/account/register') }}">
                <input class="button_nav" type="submit" value="Registriraj">
            </form>
            <!--
                            <p>Pronađite svoj smještaj</p>
                            <form action="http://www.smjestajhrvatska.com/registracija/unajmljivac.php">
                                <input class="button_nav" type="submit" value="Registriraj se">
                            </form>
            -->
        </div>

    </div>
@endif;


        <!----------------------------------   LANGUAGE   ---------------------------------->

{{--<div id="lang_dropdown">--}}
    {{--<div class="current">Hrvatski</div>--}}
    {{--<div class="nav_arrowdown_click"></div>--}}
    {{--<div class="dropdown">--}}
        {{--<a href="http://www.smjestajhrvatska.com/" title="SmjestajHrvatska.com - Hrvatski" class="hr">Hrvatski</a>--}}
        {{--<a href="http://www.smjestajhrvatska.com/en" title="SmjestajHrvatska.com - English" class="en">English</a>--}}
        {{--<a href="http://www.unterkunftkroatien.com/" title="UnterkunftKroatien.com - Deutsch" class="de">Deutsch</a>--}}
    {{--</div>--}}
{{--</div>--}}


<div id="close_dropdown"></div>
<!-- InstanceBeginEditable name="HIDE - SHOW" -->

    @yield('hide-show')

<!-- InstanceEndEditable -->
<div id="content_area">
    <!-- InstanceBeginEditable name="CONTENT AREA" -->

    @yield('content')



    <!-- InstanceEndEditable -->
</div>


<div id="footer">
    <div class="limiter">
        <div class="to_top" onclick="$('html,body').animate({ scrollTop: 0 }, '1500');">
            <div class="hide_shadow"></div>
            <div class="arrow_up"></div>
        </div>
        <div class="footerline first">
            <div class="title"><a href="{{ URL::to('/map') }}?map=ap">Apartmani</a></div>
            <ul>
                <li><a href="{{ URL::to('/map') }}?map=ap-is">Apartmani u Istri</a></li>
                <li><a href="{{ URL::to('/map') }}?map=ap-kv">Apartmani na Kvarneru</a></li>
                <li><a href="{{ URL::to('/map') }}?map=ap-da">Apartmani u Dalmaciji</a></li>
                <li><a href="{{ URL::to('/map') }}?map=ap-ko">Apartmani u Kontinentalnoj
                        Hrvatskoj</a></li>
            </ul>
        </div>
        <div class="footerline">
            <div class="title"><a href="{{ URL::to('/map') }}?map=so">Sobe</a></div>
            <ul>
                <li><a href="{{ URL::to('/map') }}?map=so-is">Sobe u Istri</a></li>
                <li><a href="{{ URL::to('/map') }}?map=so-kv">Sobe na Kvarneru</a></li>
                <li><a href="{{ URL::to('/map') }}?map=so-da">Sobe u Dalmaciji</a></li>
                <li><a href="{{ URL::to('/map') }}?map=so-ko">Sobe u Kontinentalnoj Hrvatskoj</a>
                </li>
            </ul>
        </div>
        <div class="footerline">
            <div class="title"><a href="{{ URL::to('/map') }}?map=st">Studio apartmani</a></div>
            <ul>
                <li><a href="{{ URL::to('/map') }}?map=st-is">Studio apartmani u Istri</a></li>
                <li><a href="{{ URL::to('/map') }}?map=st-kv">Studio apartmani na Kvarneru</a></li>
                <li><a href="{{ URL::to('/map') }}?map=st-da">Studio apartmani u Dalmaciji</a></li>
                <li><a href="{{ URL::to('/map') }}?map=st-ko">Studio apartmani u Kontinentalnoj Hrvatskoj</a></li>
            </ul>
        </div>
        <div class="footerline last">
            <div class="title"><a href="{{ URL::to('/map') }}?map=ku">Kuće za odmor</a></div>
            <ul>
                <li><a href="{{ URL::to('/map') }}?map=ku-is">Kuće u Istri</a></li>
                <li><a href="{{ URL::to('/map') }}?map=ku-kv">Kuće na Kvarneru</a></li>
                <li><a href="{{ URL::to('/map') }}?map=ku-da">Kuće u Dalmaciji</a></li>
                <li><a href="{{ URL::to('/map') }}?map=ku-ko">Kuće u Kontinentalnoj Hrvatskoj</a>
                </li>
            </ul>
        </div>


        {{--<div class="closingline">--}}
            {{--<div class="text">© 2015 SmjestajHrvatska.com, sva prava pridržana</div>--}}
            {{--<ul>--}}
                {{--<li><a target="_blank"--}}
                       {{--href="http://www.smjestajhrvatska.com/registracija/uvjeti/uvjeti_za_unajmljivace.html"><span>/ </span>Opći--}}
                        {{--uvjeti poslovanja za unajmljivače</a></li>--}}
                {{--<li><a target="_blank"--}}
                       {{--href="http://www.smjestajhrvatska.com/registracija/uvjeti/uvjeti_za_iznajmljivace.html"><span>/ </span>Opći--}}
                        {{--uvjeti poslovanja za iznajmljivače</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    </div>
</div>


</body>
<!-- InstanceEnd -->
</html>