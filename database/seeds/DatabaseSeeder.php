<?php

use App\Zavrsni\Account\Seeder\RoleSeeder;
use App\Zavrsni\Account\Seeder\UserSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);

        Model::reguard();
    }
}
