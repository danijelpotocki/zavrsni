<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartman_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apartman_id')->unsigned();
            $table->tinyInteger('tv');
            $table->tinyInteger('satellite');
            $table->tinyInteger('dvd');
            $table->tinyInteger('internet');
            $table->tinyInteger('radio');
            $table->tinyInteger('cooler');
            $table->tinyInteger('freezer');
            $table->tinyInteger('stove');
            $table->tinyInteger('stove2');
            $table->tinyInteger('barbecue');
            $table->tinyInteger('aircondition');
            $table->tinyInteger('washer');
            $table->tinyInteger('bathtub');
            $table->tinyInteger('shower');
            $table->tinyInteger('sea_view');
            $table->tinyInteger('parking');
            $table->tinyInteger('garage');
            $table->tinyInteger('pool');
            $table->tinyInteger('terrace');
            $table->tinyInteger('breakfast');
            $table->tinyInteger('smoking');
            $table->tinyInteger('pet');
            $table->tinyInteger('disabled_person');
            $table->tinyInteger('tax');
            $table->text('additional_price');
            $table->text('description');
            $table->text('reserved');
            $table->timestamps();

            $table->foreign('apartman_id', 'fk_apartman_details1_idx')
                ->references('id')
                ->on('apartman')
                ->onUpdate('cascade')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apartman_details');
    }
}
