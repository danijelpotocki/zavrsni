<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('email')->unique();
            $table->string('name');
            $table->string('surname');
            $table->string('phone');
            $table->string('confimation_code')->nullable();
            $table->string('password');
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('role_id')->unsigned()->nullable();
            $table->tinyInteger('is_active')->default(0);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('country_id', 'fk_user_country1_idx')
                ->references('id')
                ->on('country')
                ->onUpdate('cascade')
                ->onDelete(null)
            ;

            $table->foreign('role_id', 'fk_user_role1_idx')
                ->references('id')
                ->on('role')
                ->onUpdate('cascade')
                ->onDelete(null)
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
