<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealEstateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_estate_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('real_estate_id')->unsigned();
            $table->string('distance_center');
            $table->string('distance_store');
            $table->string('distance_disco');
            $table->string('distance_beach');
            $table->string('distance_restoran');
            $table->string('distance_transport');
            $table->string('distance_ferry_city');
            $table->string('distance_ferry');
            $table->string('distance_plane_city');
            $table->string('distance_plane');
            $table->tinyInteger('car');
            $table->tinyInteger('transport');
            $table->tinyInteger('main_road');
            $table->tinyInteger('road_beach');
            $table->tinyInteger('near_beach');
            $table->tinyInteger('quiet');
            $table->tinyInteger('alone');
            $table->tinyInteger('green');
            $table->tinyInteger('pebble');
            $table->tinyInteger('sand');
            $table->tinyInteger('concrete');
            $table->tinyInteger('rock');
            $table->text('description');
            $table->timestamps();

            $table->foreign('real_estate_id', 'fk_real_estate_details1_idx')
                ->references('id')
                ->on('real_estate')
                ->onUpdate('cascade')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('real_estate_details');
    }
}
