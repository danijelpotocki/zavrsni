<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('real_estate_id')->unsigned();
            $table->string('type');
            $table->integer('level');
            $table->string('bed');
            $table->string('room');
            $table->text('dates');
            $table->text('people');
            $table->text('minimum_people');
            $table->text('price');
            $table->string('price_type');
            $table->decimal('provision', 6, 4);
            $table->tinyInteger('is_active');
            $table->timestamps();

            $table->foreign('real_estate_id', 'fk_real_estate_apartman1_idx')
                ->references('id')
                ->on('real_estate')
                ->onUpdate('cascade')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apartman');
    }
}
