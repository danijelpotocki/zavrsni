<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('region_id')->unsigned()->nullable()->default(null);
            $table->integer('zoom_level');
            $table->decimal('lat', 8, 6);
            $table->decimal('long', 8, 6);
            $table->timestamps();

            $table->foreign('region_id', 'fk_city_region1_idx')
                ->references('id')
                ->on('region')
                ->onUpdate('cascade')
                ->onDelete(null)
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('city');
    }
}
