<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealEstateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_estate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->string('address');
            $table->string('postal_code');
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('type');
            $table->decimal('lat', 8, 6);
            $table->decimal('long', 8, 6);
            $table->text('description');
            $table->timestamps();

            $table->foreign('user_id', 'fk_user_real_estate1_idx')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('cascade')
            ;

            $table->foreign('city_id', 'fk_city_real_estate1_idx')
                ->references('id')
                ->on('city')
                ->onUpdate('cascade')
                ->onDelete(null)
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('real_estate');
    }
}
