var map;
var lat;
var lng;

function lokacija(lat, lng) {
	var Latlng = new google.maps.LatLng(lat,lng);
	// == tu je array za izgleda karte po zelji, kopira se JSON ==
		var styles = [ { "featureType": "poi.business", "stylers": [ { "visibility": "off" } ] } ];

	// == stvaranje stila karte ==
		var styledMap = new google.maps.StyledMapType(styles,
			{name: "Karta"});

	// == stvaranje karte i opcija koje su na karte ==
		var mapOptions = {
				zoom: 16,
				center: Latlng,
				mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.HYBRID, 'map_style']
				}
			};
			
		map = new google.maps.Map(document.getElementById("lokacija"),mapOptions);
		sidemap = new google.maps.Map(document.getElementById("sidemap"),mapOptions);
				
	//Associate the styled map with the MapTypeId and set it to display.
			map.mapTypes.set('map_style', styledMap);
			map.setMapTypeId(google.maps.MapTypeId.HYBRID);

			sidemap.mapTypes.set('map_style', styledMap);
			sidemap.setMapTypeId('map_style');

	// == Onemoguci scrollanje stranice kada je mis unutar karte ==
		var oldScrollTop, body = document.documentElement  ||  document.body;
		map.getDiv().addEventListener("DOMMouseScroll", function (e) {
			if (e.axis === e.VERTICAL_AXIS) {
				oldScrollTop = body.scrollTop;
				setTimeout( function () { body.scrollTop = oldScrollTop; } );
			}
		}, true);
	
	// == Onemoguci scrollanje stranice kada je mis unutar karte ==
		sidemap.getDiv().addEventListener("DOMMouseScroll", function (e) {
			if (e.axis === e.VERTICAL_AXIS) {
				oldScrollTop = body.scrollTop;
				setTimeout( function () { body.scrollTop = oldScrollTop; } );
			}
		}, true);
		
	// == provjerava granice karte ==	
		google.maps.event.addListener(map, "idle", function(){
		google.maps.event.trigger(map, 'resize'); 
		});

	// == provjerava granice karte ==	
		google.maps.event.addListener(sidemap, "idle", function(){
		google.maps.event.trigger(sidemap, 'resize'); 
		});	


	
	  var marker = new google.maps.Marker({
	      position: Latlng,
	      map: map,
		  icon: 'http://smjestajhrvatska.com/images/map_icons/map_privatni.png'
	  });
	  
	  
	  var marker = new google.maps.Marker({
	      position: Latlng,
	      map: sidemap,
		  icon: 'http://smjestajhrvatska.com/images/map_icons/map_privatni.png',
	  });
}