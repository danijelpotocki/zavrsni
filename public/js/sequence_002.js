$(document).ready(function(){
	var sequence = $("#sequence").sequence(options).data("sequence");

	var options = {
		autoPlay: true,
		autoPlayDelay: 10,
		pauseOnHover: false,
		navigationSkipThreshold: 10,
		fadeFrameWhenSkipped: true
	};
});