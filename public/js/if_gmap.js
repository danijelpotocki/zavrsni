/* Code based on Google Map APIv3 Tutorials */
window.onload=function(){
	if_gmap_init();
};


var gmapdata;
var gmapmarker;
var infoWindow;

var def_zoomval = 7;
var def_longval = 16.430054;
var def_latval = 44.621754;

function if_gmap_init()
{
	var curpoint = new google.maps.LatLng(def_latval,def_longval);
  var mapOptions = {
    zoom: def_zoomval,
    center: curpoint,
    mapTypeId: google.maps.MapTypeId.HYBRID
  };
  

  
  gmapdata = new google.maps.Map(document.getElementById('mapitems'), mapOptions);

  
  /*
		// == tu je array za izgleda karte po zelji, kopira se JSON ==
		var styles = [ { "featureType": "poi.business", "stylers": [ { "visibility": "off" } ] } ];

	// == stvaranje stila karte ==
		var styledMap = new google.maps.StyledMapType(styles,
			{name: "Karta"});
			
	gmapdata = new google.maps.Map(document.getElementById("mapitems"), {
		center: curpoint,
		zoom: def_zoomval,
		mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.HYBRID, 'map_style']
				}
		});*/

	gmapmarker = new google.maps.Marker({
		map: gmapdata,
		icon: 'http://smjestajhrvatska.com/images/map_icons/map_prazan.png'
				});/*
				
	//Associate the styled map with the MapTypeId and set it to display.
			gmapdata.mapTypes.set('map_style', styledMap);
			gmapdata.setMapTypeId('map_style');*/


	
// == Onemoguci scrollanje stranice kada je mis unutar karte ==
	var oldScrollTop, body = document.documentElement  ||  document.body;
	gmapdata.getDiv().addEventListener("DOMMouseScroll", function (e) {
		if (e.axis === e.VERTICAL_AXIS) {
			oldScrollTop = body.scrollTop;
			setTimeout( function () { body.scrollTop = oldScrollTop; } );
		}
	}, true);
		
		
		
// == provjerava granice karte ==	
	google.maps.event.addListener(gmapdata, "idle", function(){
	google.maps.event.trigger(gmapdata, 'resize'); 
	});
	

	google.maps.event.addListener(gmapdata, 'click', function(event) {
		document.getElementById("longval").value = event.latLng.lng().toFixed(6);
		document.getElementById("latval").value = event.latLng.lat().toFixed(6);
		gmapmarker.setPosition(event.latLng);
	});


	document.getElementById("longval").value = def_longval;
	document.getElementById("latval").value = def_latval;

	return false;
} // end of if_gmap_init


function mapCenter(mjesto){
	var koordinate = mjesto.split(",");
	gmapdata.setCenter(new google.maps.LatLng(+koordinate[1],+koordinate[2]));
	gmapdata.setZoom(+koordinate[3]);
}