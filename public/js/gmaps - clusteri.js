	var gmarkers = [];
	var map;
	var karta;
	var zoom;
    var markerCluster = null;
	var mcOptions = {gridSize: 70, maxZoom: 15};
	var boxText = document.createElement("div");
	var zoom;
	var date1;
	var date2;
	var osoba;
	


																																																																																					
	// == izgled ikona ==
    var customIcons = {
      privatni: {
        icon: 'http://smjestajhrvatska.com/images/map_icons/map_privatni.png'
      },
      hotel: {
        icon: 'http://smjestajhrvatska.com/images/map_icons/map_hoteli.png'
      },
      tnaselje: {
        icon: 'http://smjestajhrvatska.com/images/map_icons/map_prazan.png'
      }
    };

	
	//== stil karte =
    function gmaps(lat, lng, zoom) {

		
	// == tu je array za izgleda karte po zelji, kopira se JSON ==
		var styles = [ { "featureType": "poi.business", "stylers": [ { "visibility": "off" } ] } ];

	// == stvaranje stila karte ==
		var styledMap = new google.maps.StyledMapType(styles,
			{name: "Karta"});

	// == stvaranje karte i opcija koje su na karte ==
		var mapOptions = {
				zoom: zoom,
				center: new google.maps.LatLng(lat, lng),
				mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.HYBRID, 'map_style']
				}
			};
			
		map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);

				
	//Associate the styled map with the MapTypeId and set it to display.
			map.mapTypes.set('map_style', styledMap);
			map.setMapTypeId('map_style');

	// == Onemoguci scrollanje stranice kada je mis unutar karte ==
		var oldScrollTop, body = document.documentElement  ||  document.body;
		map.getDiv().addEventListener("DOMMouseScroll", function (e) {
			if (e.axis === e.VERTICAL_AXIS) {
				oldScrollTop = body.scrollTop;
				setTimeout( function () { body.scrollTop = oldScrollTop; } );
			}
		}, true);
		
		
		
	// == provjerava granice karte ==	
		google.maps.event.addListener(map, "idle", function(){
		google.maps.event.trigger(map, 'resize'); 
		});

	
	dohvatimarkere(0,0,0);
    }
	
	
	
	function dohvatimarkere(date1, date2, osoba){
	
		gmarkers = [];
		//markerCluster.clearMarkers();
		
	    // == citanje iz baze podataka i pozivanje funkcije da napravi marker ==
      downloadUrl("../config/getmarkers.php?date1="+date1+"&date2="+date2+"&osoba="+osoba, function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var ime = markers[i].getAttribute("Id");
          var vrsta = markers[i].getAttribute("vrsta");
          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));
		
		  var html ='<iframe style="background-image:url(http://www.smjestajhrvatska.com/images/loading.gif); background-repeat:no-repeat; background-position:center 70px; position:absolute; border:0px; overflow:hidden; width:353px; height:165px;" src="http://www.smjestajhrvatska.com/pretrazi/infobox.php?id='+ime+'"></iframe>';

          var icon = customIcons[vrsta] || {};
		  
		  addMarker(vrsta, point, icon, map, html);		
		
        }
		
		show("privatni");

      });

	}
	
	function advanceMarkers(date1, date2, osoba, price_min, price_max, room, box){

		gmarkers = [];
		//gmarkers.infobox.close();
	    // == citanje iz baze podataka i pozivanje funkcije da napravi marker ==
     downloadUrl("../config/getmarkers_advance.php?date1="+date1+"&date2="+date2+"&osoba="+osoba+"&price_min="+price_min+"&price_max="+price_max+"&room="+room+"&apartman="+box[0]+"&soba="+box[1]+"&studio="+box[2]+"&kuca="+box[3]+"&hotel="+box[4]+"&more="+box[5]+"&klima="+box[6]+"&tv="+box[7]+"&internet="+box[8]+"&parking="+box[9]+"&bazen="+box[10]+"&hladnjak="+box[11]+"&stednjak="+box[12]+"&rostilj="+box[13]+"&ljubimci="+box[14]+"&pusenje="+box[15], function(data) {
		var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var ime = markers[i].getAttribute("Id");
          var vrsta = markers[i].getAttribute("vrsta");
          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));
		
		  var html ='<iframe style="background-image:url(http://www.smjestajhrvatska.com/images/loading.gif); background-repeat:no-repeat; background-position:center 70px; position:absolute; border:0px; overflow:hidden; width:353px; height:165px;" src="http://www.smjestajhrvatska.com/pretrazi/infobox.php?id='+ime+'"></iframe>';

          var icon = customIcons[vrsta] || {};
		  
		  addMarker(vrsta, point, icon, map, html);		
		
        }
		
		show("privatni");
		

		
      });
	
	}
	
	function newCenter(centerlat,centerlng, zoom){
		map.setCenter(new google.maps.LatLng(centerlat,centerlng));
		map.setZoom(zoom);
	}
	
	
	// == stvaranje markera i infowindow ==
    function addMarker(vrsta, point, icon, map, html) {
			var contentString = html;
			var marker = new google.maps.Marker({
				map: map,
				position: point,
				icon: icon.icon,
				shadow: icon.shadow
			});
		  
            google.maps.event.addListener(marker, 'mouseover', function () {
                this.setOptions({zIndex:10});
            });
            google.maps.event.addListener(marker, 'mouseout', function () {
                this.setOptions({zIndex:1});
            });
			
            //these are the options for all infoboxes
            infoboxOptions = {
                content: boxText,
                disableAutoPan: false,
                pixelOffset: new google.maps.Size(-50, -201),
                zIndex: null,
                boxStyle: {
                    opacity: 1,
                	zIndex: 200,
                },
				closeBoxMargin: "0px 0px 0px 0px",
				closeBoxURL: "http://www.smjestajhrvatska.com/images/close16.png",
				infoBoxClearance: new google.maps.Size(1, 1),
				isHidden: false,
				pane: "floatPane",
				enableEventPropagation: false
            };	  
		marker.vrsta = vrsta;
		gmarkers.push(marker);

		boxText.style.cssText = "width:353px; height:163px; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; -webkit-box-shadow:#8f969c 0px 0px 3px 0px; -moz-box-shadow:#8f969c 0px 0px 3px 0px; box-shadow:#8f969c 0px 0px 3px 0px; background:#fff; border:3px solid #fff; margin:10px 10px 0px 10px;";

			
		gmarkers.infobox = new InfoBox(infoboxOptions);
		
		google.maps.event.addListener(marker, 'click', function() {
		boxText.innerHTML = html;
		//gmarkers.infobox.close();
        gmarkers.infobox.open(map, this);
        });
		
      }
	  
		
     // == prikazi sve markere iz kategorije i stavi kvacicu ==
      function show(vrsta) {
        for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].vrsta == vrsta) {
            gmarkers[i].setVisible(true);
          }
        }
    // == postavi kvacicu ==
        //document.getElementById(vrsta+"box").checked = true;
		
    // create temp array for cluster
        var temp_markers = new Array();
    // if the markerClusterer object doesn't exist, create it with empty temp_markers
        if (markerCluster == null) {
            markerCluster = new MarkerClusterer(map, temp_markers, mcOptions);
        }
        
        // clear all markers
        markerCluster.clearMarkers();
 
        // iterate through all locations, setting only those in the selected category
		for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].getVisible()) {
             temp_markers.push(gmarkers[i]);
          }
		  }// for
        
        /// add all current markers to cluster
        markerCluster.addMarkers(temp_markers);
		
      }

      // == sakrij sve markere iz kazegorije i makni kvacicu ==
      function hide(vrsta) {
        for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].vrsta == vrsta) {
            gmarkers[i].setVisible(false);
          }
        }
        // == makni kvacicu ==
        document.getElementById(vrsta+"box").checked = false;
        // == ako je infowindow bio otvoren a ugasio si kategoriju nestat ce info

		gmarkers.infobox.close();


        // create temp array for cluster
        var temp_markers = new Array();
        // if the markerClusterer object doesn't exist, create it with empty temp_markers
        if (markerCluster == null) {
            markerCluster = new MarkerClusterer(map, temp_markers, mcOptions);
        }
        
        // clear all markers
        markerCluster.clearMarkers();
 
        // iterate through all locations, setting only those in the selected category
		for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].getVisible()) {
             temp_markers.push(gmarkers[i]);
          }
		  }// for
        
        /// add all current markers to cluster
        markerCluster.addMarkers(temp_markers);
      }

      // == stavi/makni kvacicu ==
      function boxclick(box,vrsta) {
        if (box.checked) {
          show(vrsta);
        } else {
          hide(vrsta);
        }
      }

	  
	  // = pozivanje php datoteke koja SQL bazu pretvara u XML ==
    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }

    function doNothing() {}
