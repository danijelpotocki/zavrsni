
window.onload=function(){
	
	// IE 9 OR OLDER
	
	if ($.browser.msie && parseInt($.browser.version, 10) < 9){		
		$(".old_IE").show();
	}
};


$(document).ready(function() {

	// METANAV

	if ( $("#metanav").length ) {
		$("#metanav > li").hover(function() {

			var thisposition = $(this).position().left;
			var thiswidth = $(this).width();
			var posfromleft = thisposition + ((thiswidth / 2) - 7);
			var child = $(".dropdown",$(this));
			var arrow = $(".arrow_down",$(this));
			arrow.css("left",posfromleft);

			if ( child.hasClass("dropdown") ) {
				var siteheight = $("html").height();
				arrow.show();
				child.fadeIn(250);
			}

		},		
		
		function() {
			var child = $(".dropdown",$(this));
			var arrow = $(".arrow_down",$(this));
			child.hide();
			arrow.hide();
		});
	}




	// SEARCH TYPE OF SERVICE

	if ( $(".linkblock").length ) {
		$(".linkblock > li").click(function() {
			
			var child = $(".linkblock_slide",$(this));
			var option = $(".option",$(this));

			if ( child.hasClass("linkblock_slide") ) {
				$(".linkblock_slide",".linkblock").removeClass("active");
				$(".option",".linkblock").removeClass("active");
				$(child).addClass("active");
				$(option).addClass("active");
			}

		});
	}
			

});






$(window).load(function() {
	


	// LOGIN/REGISTER DROPDOWN
	
	$("#login_register_dropdown").click(function () {
		$("#login_register_dropdown").addClass("active");
		$("#close_dropdown").show();
		$(".nav_arrowdown_click","#login_register_dropdown").show();
		$(".dropdown","#login_register_dropdown").slideDown();
	});


	// USER
	
	$("#user_dropdown").click(function () {
		$("#user_dropdown").addClass("active");
		$("#close_dropdown").show();
		$(".nav_arrowdown_click","#user_dropdown").show();
		$(".dropdown","#user_dropdown").slideDown();
	});

		
	// LANGUAGE DROPDOWN
	
	$("#lang_dropdown").click(function () {
		$("#lang_dropdown").addClass("active");
		$("#close_dropdown").show();
		$(".nav_arrowdown_click","#lang_dropdown").show();
		$(".dropdown","#lang_dropdown").slideDown();
	});


	// CLOSE DROPDOWN
			
	$("#close_dropdown").click(function () {
		$("#login_register_dropdown").removeClass("active");
		$("#close_dropdown").hide();
		$(".nav_arrowdown_click","#login_register_dropdown").hide();
		$(".dropdown","#login_register_dropdown").slideUp();

		$("#user_dropdown").removeClass("active");
		$("#close_dropdown").hide();
		$(".nav_arrowdown_click","#user_dropdown").hide();
		$(".dropdown","#user_dropdown").slideUp();

		$("#lang_dropdown").removeClass("active");
		$("#close_dropdown").hide();
		$(".nav_arrowdown_click","#lang_dropdown").hide();
		$(".dropdown","#lang_dropdown").slideUp();
	});


		

});













