$(document).ready(function() {


	var mjesto = window.localStorage.getItem("mjesto");
	var osoba = window.localStorage.getItem("osoba");
	var date1 = window.localStorage.getItem("date1");
	var date2 = window.localStorage.getItem("date2");
	
	if(!(mjesto==null || mjesto=="" || mjesto=='null')){
		mjesto = mjesto.split(",");
		mjesto = mjesto[3];
		document.getElementById('mjesto').value = mjesto;
	}
	
	if(!(osoba==null || osoba=="" || osoba=='null')){
		document.getElementById('osoba').value = osoba;
	}
	
	if(!(date1==null || date1=="" || date1=='null')){
		document.getElementById('datepick13').value = date1;
	}
	
	if(!(date2==null || date2=="" || date2=='null')){
		document.getElementById('datepick14').value = date2;
	}
	
	window.localStorage.setItem("mjesto", 'null');
	window.localStorage.setItem("osoba", 'null');
	window.localStorage.setItem("date1", 'null');
	window.localStorage.setItem("date2", 'null');
	window.localStorage.setItem("page", 1);
	//generate_list();
});

function generate_list(pageReset){

	if(pageReset){
		window.localStorage.setItem("page", 1);
	} 

	var page = window.localStorage.getItem("page");
	var mjesto = document.getElementById('mjesto');
	var date1 = document.getElementById('datepick13');
	var date2 = document.getElementById('datepick14');
	var osoba = document.getElementById('osoba');	
	var price_min = document.getElementById('price_min');	
	var price_max = document.getElementById('price_max');
	var room = document.getElementById('room');
	var akcija = document.getElementById('akcija').checked;

	var box = [];	

	for (var i=1;i<=16;i++){
		if(document.getElementById('box_' + i)){
				box.push(document.getElementById('box_' + i).checked);
		}else{
			box.push(false);
		}

	}

	$.post('../config/list_generate.php',
		{
		page : page,
		mjesto : mjesto.value,
		date1 : date1.value,
		date2 : date2.value,
		osoba : osoba.value,
		price_min : price_min.value,
		price_max : price_max.value,
		room : room.value,
		akcija : akcija,
		apartman : box[0],
		soba : box[1],
		studio : box[2],
		kuca : box[3],
		hotel : box[4],
		more : box[5],
		klima : box[6],
		tv : box[7],
		internet : box[8],
		parking : box[9],
		bazen : box[10],
		hladnjak : box[11],
		stednjak : box[12],
		rostilj : box[13],
		ljubimci : box[14],
		pusenje : box[15]
		},
		function(data) {
			$('#lista').fadeIn(750).html(data);
		}
	);

}

function page_next(){
	$('#lista').hide();
	var page = window.localStorage.getItem("page");
	window.localStorage.setItem("page", (+page+1));
	generate_list();
}

function page_back(){
	$('#lista').hide();
	var page = window.localStorage.getItem("page");
	window.localStorage.setItem("page", (+page-1));
	generate_list();
}

function page_change(page){
	$('#lista').hide();
	window.localStorage.setItem("page", page);
	generate_list();
}

var t;
function SearchDelayList(delay){

	if(delay){
		if ( t ){
			clearTimeout( t );
			t = setTimeout( function (){
								$('#lista').hide();
								window.localStorage.setItem("page", 1);
								generate_list();
								}, 500 );
		}else{
			t = setTimeout( function (){
								$('#lista').hide();
								window.localStorage.setItem("page", 1);
								generate_list();
								}, 500 );
		}
	}else{
		$('#lista').hide();
		window.localStorage.setItem("page", 1);	
		generate_list();
	}
	
}

