/* --------------------- WEATHER ZAGREB --------------------- */

$(function weather_zagreb(){
	
	/* Configuration */
	
	var APPID = 'CwCOKf32';		// Your Yahoo APP id
	var DEG = 'c';		// c for celsius, f for fahrenheit
	
	// Mapping the weather codes returned by Yahoo's API
	// to the correct icons in the img/icons folder
	
	var weatherIconMap = [
		'storm', 'storm', 'storm', 'lightning', 'lightning', 'snow', 'hail', 'hail',
		'drizzle', 'drizzle', 'rain', 'rain', 'rain', 'snow', 'snow', 'snow', 'snow',
		'hail', 'hail', 'fog', 'fog', 'fog', 'fog', 'wind', 'wind', 'snowflake',
		'cloud', 'cloud_moon', 'cloud_sun', 'cloud_moon', 'cloud_sun', 'moon', 'sun',
		'moon', 'sun', 'hail', 'sun', 'lightning', 'lightning', 'lightning', 'rain',
		'snowflake', 'snowflake', 'snowflake', 'cloud', 'rain', 'snow', 'lightning'
	];
	
	var weatherDiv = $('#weather_zagreb'),
		scroller = $('#scroller_zagreb'),
		location = $('p.location_zagreb');
	

	
	/* Configuration */
	
	zagreb(); 
	
	function zagreb() {
	    var lat = 45.79434;
	    var lon = 15.99060;

	    // We are passing the R gflag for reverse geocoding (coordinates to place name)
	    var geoAPI = 'http://where.yahooapis.com/geocode?location='+lat+','+lon+'&flags=J&gflags=R&appid='+APPID;
	    
	    // Forming the query for Yahoo's weather forecasting API with YQL
	    // http://developer.yahoo.com/weather/

	    var wsql = 'select * from weather.forecast where woeid=WID and u="'+DEG+'"',
	        weatherYQL = 'http://query.yahooapis.com/v1/public/yql?q='+encodeURIComponent(wsql)+'&format=json&callback=?',
	        code, city, results, woeid;
	    
	    if (window.console && window.console.info){
	    	console.info("Coordinates: %f %f", lat, lon);
	    }
	    
	    // Issue a cross-domain AJAX request (CORS) to the GEO service.
	    // Not supported in Opera and IE.
	    $.getJSON(geoAPI, function(r){
	       
	        if(r.ResultSet.Found == 1){
	        	
	            results = r.ResultSet.Results;
	            city = results[0].city;
	            code = results[0].statecode || results[0].countrycode;
	    
	            // This is the city identifier for the weather API
	            woeid = results[0].woeid;
	
	            // Make a weather API request:
	            $.getJSON(weatherYQL.replace('WID',woeid), function(r){
	            	
	                if(r.query && r.query.count == 1){
	                	
	                	// Create the weather items in the #scroller UL
	                	
	                    var item = r.query.results.channel.item.condition;
	                    
	                    if(!item){
	                    	showError("We can't find weather information about your city!");
	                    	if (window.console && window.console.info){
						    	console.info("%s, %s; woeid: %d", city, code, woeid);
						    }
						    
						    return false;
	                    }
	                    
	                    addWeather_zagreb(item.code, "LIVE", ' <b>'+item.temp+'°'+DEG+'</b>');
	                    
	                    // Add the location to the page
	                    location.html();
	                    weatherDiv.addClass('loaded');
	                   	               
	                }
	            });
	    
	        }
	        
	    }).error(function(){
	    	showError("Your browser does not support CORS requests!");
	    });
	   
	}
	
	function addWeather_zagreb(code, day, condition){
		
	    var markup = '<li>'+
	    	'<img src="images/weather/'+ weatherIconMap[code] +'.png" />'+
	    	' <p class="day">'+ day +'</p> <p class="cond">'+ condition +
	    	'</p></li>';
	    
	    scroller.append(markup);
	}

});








/* --------------------- WEATHER SREDISNJA HR --------------------- */

$(function weather_sredisnjahr(){
	
	/* Configuration */
	
	var APPID = 'CwCOKf32';		// Your Yahoo APP id
	var DEG = 'c';		// c for celsius, f for fahrenheit
	
	// Mapping the weather codes returned by Yahoo's API
	// to the correct icons in the img/icons folder
	
	var weatherIconMap = [
		'storm', 'storm', 'storm', 'lightning', 'lightning', 'snow', 'hail', 'hail',
		'drizzle', 'drizzle', 'rain', 'rain', 'rain', 'snow', 'snow', 'snow', 'snow',
		'hail', 'hail', 'fog', 'fog', 'fog', 'fog', 'wind', 'wind', 'snowflake',
		'cloud', 'cloud_moon', 'cloud_sun', 'cloud_moon', 'cloud_sun', 'moon', 'sun',
		'moon', 'sun', 'hail', 'sun', 'lightning', 'lightning', 'lightning', 'rain',
		'snowflake', 'snowflake', 'snowflake', 'cloud', 'rain', 'snow', 'lightning'
	];
	
	var weatherDiv = $('#weather_sredisnjahr'),
		scroller = $('#scroller_sredisnjahr'),
		location = $('p.location_sredisnjahr');
	

	
	/* Configuration */
	
	sredisnjahr(); 
	
	function sredisnjahr() {
	    var lat = 45.70714;
	    var lon = 16.07300;

	    // We are passing the R gflag for reverse geocoding (coordinates to place name)
	    var geoAPI = 'http://where.yahooapis.com/geocode?location='+lat+','+lon+'&flags=J&gflags=R&appid='+APPID;
	    
	    // Forming the query for Yahoo's weather forecasting API with YQL
	    // http://developer.yahoo.com/weather/

	    var wsql = 'select * from weather.forecast where woeid=WID and u="'+DEG+'"',
	        weatherYQL = 'http://query.yahooapis.com/v1/public/yql?q='+encodeURIComponent(wsql)+'&format=json&callback=?',
	        code, city, results, woeid;
	    
	    if (window.console && window.console.info){
	    	console.info("Coordinates: %f %f", lat, lon);
	    }
	    
	    // Issue a cross-domain AJAX request (CORS) to the GEO service.
	    // Not supported in Opera and IE.
	    $.getJSON(geoAPI, function(r){
	       
	        if(r.ResultSet.Found == 1){
	        	
	            results = r.ResultSet.Results;
	            city = results[0].city;
	            code = results[0].statecode || results[0].countrycode;
	    
	            // This is the city identifier for the weather API
	            woeid = results[0].woeid;
	
	            // Make a weather API request:
	            $.getJSON(weatherYQL.replace('WID',woeid), function(r){
	            	
	                if(r.query && r.query.count == 1){
	                	
	                	// Create the weather items in the #scroller UL
	                	
	                    var item = r.query.results.channel.item.condition;
	                    
	                    if(!item){
	                    	showError("We can't find weather information about your city!");
	                    	if (window.console && window.console.info){
						    	console.info("%s, %s; woeid: %d", city, code, woeid);
						    }
						    
						    return false;
	                    }
	                    
	                    addWeather_sredisnjahr(item.code, "LIVE", ' <b>'+item.temp+'°'+DEG+'</b>');
	                    
	                    // Add the location to the page
	                    location.html();
	                    weatherDiv.addClass('loaded');
	                   	               
	                }
	            });
	    
	        }
	        
	    }).error(function(){
	    	showError("Your browser does not support CORS requests!");
	    });
	   
	}
	
	function addWeather_sredisnjahr(code, day, condition){
		
	    var markup = '<li>'+
	    	'<img src="images/weather/'+ weatherIconMap[code] +'.png" />'+
	    	' <p class="day">'+ day +'</p> <p class="cond">'+ condition +
	    	'</p></li>';
	    
	    scroller.append(markup);
	}

});








/* --------------------- WEATHER SLAVONIJA --------------------- */

$(function weather_slavonija(){
	
	/* Configuration */
	
	var APPID = 'CwCOKf32';		// Your Yahoo APP id
	var DEG = 'c';		// c for celsius, f for fahrenheit
	
	// Mapping the weather codes returned by Yahoo's API
	// to the correct icons in the img/icons folder
	
	var weatherIconMap = [
		'storm', 'storm', 'storm', 'lightning', 'lightning', 'snow', 'hail', 'hail',
		'drizzle', 'drizzle', 'rain', 'rain', 'rain', 'snow', 'snow', 'snow', 'snow',
		'hail', 'hail', 'fog', 'fog', 'fog', 'fog', 'wind', 'wind', 'snowflake',
		'cloud', 'cloud_moon', 'cloud_sun', 'cloud_moon', 'cloud_sun', 'moon', 'sun',
		'moon', 'sun', 'hail', 'sun', 'lightning', 'lightning', 'lightning', 'rain',
		'snowflake', 'snowflake', 'snowflake', 'cloud', 'rain', 'snow', 'lightning'
	];
	
	var weatherDiv = $('#weather_slavonija'),
		scroller = $('#scroller_slavonija'),
		location = $('p.location_slavonija');
	

	
	/* Configuration */
	
	slavonija(); 
	
	function slavonija() {
	    var lat = 45.54868;
	    var lon = 18.68225;

	    // We are passing the R gflag for reverse geocoding (coordinates to place name)
	    var geoAPI = 'http://where.yahooapis.com/geocode?location='+lat+','+lon+'&flags=J&gflags=R&appid='+APPID;
	    
	    // Forming the query for Yahoo's weather forecasting API with YQL
	    // http://developer.yahoo.com/weather/

	    var wsql = 'select * from weather.forecast where woeid=WID and u="'+DEG+'"',
	        weatherYQL = 'http://query.yahooapis.com/v1/public/yql?q='+encodeURIComponent(wsql)+'&format=json&callback=?',
	        code, city, results, woeid;
	    
	    if (window.console && window.console.info){
	    	console.info("Coordinates: %f %f", lat, lon);
	    }
	    
	    // Issue a cross-domain AJAX request (CORS) to the GEO service.
	    // Not supported in Opera and IE.
	    $.getJSON(geoAPI, function(r){
	       
	        if(r.ResultSet.Found == 1){
	        	
	            results = r.ResultSet.Results;
	            city = results[0].city;
	            code = results[0].statecode || results[0].countrycode;
	    
	            // This is the city identifier for the weather API
	            woeid = results[0].woeid;
	
	            // Make a weather API request:
	            $.getJSON(weatherYQL.replace('WID',woeid), function(r){
	            	
	                if(r.query && r.query.count == 1){
	                	
	                	// Create the weather items in the #scroller UL
	                	
	                    var item = r.query.results.channel.item.condition;
	                    
	                    if(!item){
	                    	showError("We can't find weather information about your city!");
	                    	if (window.console && window.console.info){
						    	console.info("%s, %s; woeid: %d", city, code, woeid);
						    }
						    
						    return false;
	                    }
	                    
	                    addWeather_slavonija(item.code, "LIVE", ' <b>'+item.temp+'°'+DEG+'</b>');
	                    
	                    // Add the location to the page
	                    location.html();
	                    weatherDiv.addClass('loaded');
	                   	               
	                }
	            });
	    
	        }
	        
	    }).error(function(){
	    	showError("Your browser does not support CORS requests!");
	    });
	   
	}
	
	function addWeather_slavonija(code, day, condition){
		
	    var markup = '<li>'+
	    	'<img src="images/weather/'+ weatherIconMap[code] +'.png" />'+
	    	' <p class="day">'+ day +'</p> <p class="cond">'+ condition +
	    	'</p></li>';
	    
	    scroller.append(markup);
	}

});








/* --------------------- WEATHER LIKA --------------------- */

$(function weather_lika(){
	
	/* Configuration */
	
	var APPID = 'CwCOKf32';		// Your Yahoo APP id
	var DEG = 'c';		// c for celsius, f for fahrenheit
	
	// Mapping the weather codes returned by Yahoo's API
	// to the correct icons in the img/icons folder
	
	var weatherIconMap = [
		'storm', 'storm', 'storm', 'lightning', 'lightning', 'snow', 'hail', 'hail',
		'drizzle', 'drizzle', 'rain', 'rain', 'rain', 'snow', 'snow', 'snow', 'snow',
		'hail', 'hail', 'fog', 'fog', 'fog', 'fog', 'wind', 'wind', 'snowflake',
		'cloud', 'cloud_moon', 'cloud_sun', 'cloud_moon', 'cloud_sun', 'moon', 'sun',
		'moon', 'sun', 'hail', 'sun', 'lightning', 'lightning', 'lightning', 'rain',
		'snowflake', 'snowflake', 'snowflake', 'cloud', 'rain', 'snow', 'lightning'
	];
	
	var weatherDiv = $('#weather_lika'),
		scroller = $('#scroller_lika'),
		location = $('p.location_lika');
	

	
	/* Configuration */
	
	lika(); 
	
	function lika() {
	    var lat = 44.99734;
	    var lon = 15.12611;

	    // We are passing the R gflag for reverse geocoding (coordinates to place name)
	    var geoAPI = 'http://where.yahooapis.com/geocode?location='+lat+','+lon+'&flags=J&gflags=R&appid='+APPID;
	    
	    // Forming the query for Yahoo's weather forecasting API with YQL
	    // http://developer.yahoo.com/weather/

	    var wsql = 'select * from weather.forecast where woeid=WID and u="'+DEG+'"',
	        weatherYQL = 'http://query.yahooapis.com/v1/public/yql?q='+encodeURIComponent(wsql)+'&format=json&callback=?',
	        code, city, results, woeid;
	    
	    if (window.console && window.console.info){
	    	console.info("Coordinates: %f %f", lat, lon);
	    }
	    
	    // Issue a cross-domain AJAX request (CORS) to the GEO service.
	    // Not supported in Opera and IE.
	    $.getJSON(geoAPI, function(r){
	       
	        if(r.ResultSet.Found == 1){
	        	
	            results = r.ResultSet.Results;
	            city = results[0].city;
	            code = results[0].statecode || results[0].countrycode;
	    
	            // This is the city identifier for the weather API
	            woeid = results[0].woeid;
	
	            // Make a weather API request:
	            $.getJSON(weatherYQL.replace('WID',woeid), function(r){
	            	
	                if(r.query && r.query.count == 1){
	                	
	                	// Create the weather items in the #scroller UL
	                	
	                    var item = r.query.results.channel.item.condition;
	                    
	                    if(!item){
	                    	showError("We can't find weather information about your city!");
	                    	if (window.console && window.console.info){
						    	console.info("%s, %s; woeid: %d", city, code, woeid);
						    }
						    
						    return false;
	                    }
	                    
	                    addWeather_lika(item.code, "LIVE", ' <b>'+item.temp+'°'+DEG+'</b>');
	                    
	                    // Add the location to the page
	                    location.html();
	                    weatherDiv.addClass('loaded');
	                   	               
	                }
	            });
	    
	        }
	        
	    }).error(function(){
	    	showError("Your browser does not support CORS requests!");
	    });
	   
	}
	
	function addWeather_lika(code, day, condition){
		
	    var markup = '<li>'+
	    	'<img src="images/weather/'+ weatherIconMap[code] +'.png" />'+
	    	' <p class="day">'+ day +'</p> <p class="cond">'+ condition +
	    	'</p></li>';
	    
	    scroller.append(markup);
	}

});








/* --------------------- WEATHER ISTRA --------------------- */

$(function weather_istra(){
	
	/* Configuration */
	
	var APPID = 'CwCOKf32';		// Your Yahoo APP id
	var DEG = 'c';		// c for celsius, f for fahrenheit
	
	// Mapping the weather codes returned by Yahoo's API
	// to the correct icons in the img/icons folder
	
	var weatherIconMap = [
		'storm', 'storm', 'storm', 'lightning', 'lightning', 'snow', 'hail', 'hail',
		'drizzle', 'drizzle', 'rain', 'rain', 'rain', 'snow', 'snow', 'snow', 'snow',
		'hail', 'hail', 'fog', 'fog', 'fog', 'fog', 'wind', 'wind', 'snowflake',
		'cloud', 'cloud_moon', 'cloud_sun', 'cloud_moon', 'cloud_sun', 'moon', 'sun',
		'moon', 'sun', 'hail', 'sun', 'lightning', 'lightning', 'lightning', 'rain',
		'snowflake', 'snowflake', 'snowflake', 'cloud', 'rain', 'snow', 'lightning'
	];
	
	var weatherDiv = $('#weather_istra'),
		scroller = $('#scroller_istra'),
		location = $('p.location_istra');
	

	
	/* Configuration */
	
	istra(); 
	
	function istra() {
	    var lat = 45.33670;
	    var lon = 14.30712;

	    // We are passing the R gflag for reverse geocoding (coordinates to place name)
	    var geoAPI = 'http://where.yahooapis.com/geocode?location='+lat+','+lon+'&flags=J&gflags=R&appid='+APPID;
	    
	    // Forming the query for Yahoo's weather forecasting API with YQL
	    // http://developer.yahoo.com/weather/

	    var wsql = 'select * from weather.forecast where woeid=WID and u="'+DEG+'"',
	        weatherYQL = 'http://query.yahooapis.com/v1/public/yql?q='+encodeURIComponent(wsql)+'&format=json&callback=?',
	        code, city, results, woeid;
	    
	    if (window.console && window.console.info){
	    	console.info("Coordinates: %f %f", lat, lon);
	    }
	    
	    // Issue a cross-domain AJAX request (CORS) to the GEO service.
	    // Not supported in Opera and IE.
	    $.getJSON(geoAPI, function(r){
	       
	        if(r.ResultSet.Found == 1){
	        	
	            results = r.ResultSet.Results;
	            city = results[0].city;
	            code = results[0].statecode || results[0].countrycode;
	    
	            // This is the city identifier for the weather API
	            woeid = results[0].woeid;
	
	            // Make a weather API request:
	            $.getJSON(weatherYQL.replace('WID',woeid), function(r){
	            	
	                if(r.query && r.query.count == 1){
	                	
	                	// Create the weather items in the #scroller UL
	                	
	                    var item = r.query.results.channel.item.condition;
	                    
	                    if(!item){
	                    	showError("We can't find weather information about your city!");
	                    	if (window.console && window.console.info){
						    	console.info("%s, %s; woeid: %d", city, code, woeid);
						    }
						    
						    return false;
	                    }
	                    
	                    addWeather_istra(item.code, "LIVE", ' <b>'+item.temp+'°'+DEG+'</b>');
	                    
	                    // Add the location to the page
	                    location.html();
	                    weatherDiv.addClass('loaded');
	                   	               
	                }
	            });
	    
	        }
	        
	    }).error(function(){
	    	showError("Your browser does not support CORS requests!");
	    });
	   
	}
	
	function addWeather_istra(code, day, condition){
		
	    var markup = '<li>'+
	    	'<img src="images/weather/'+ weatherIconMap[code] +'.png" />'+
	    	' <p class="day">'+ day +'</p> <p class="cond">'+ condition +
	    	'</p></li>';
	    
	    scroller.append(markup);
	}

});








/* --------------------- WEATHER SDALMACIJA --------------------- */

$(function weather_sdalmacija(){
	
	/* Configuration */
	
	var APPID = 'CwCOKf32';		// Your Yahoo APP id
	var DEG = 'c';		// c for celsius, f for fahrenheit
	
	// Mapping the weather codes returned by Yahoo's API
	// to the correct icons in the img/icons folder
	
	var weatherIconMap = [
		'storm', 'storm', 'storm', 'lightning', 'lightning', 'snow', 'hail', 'hail',
		'drizzle', 'drizzle', 'rain', 'rain', 'rain', 'snow', 'snow', 'snow', 'snow',
		'hail', 'hail', 'fog', 'fog', 'fog', 'fog', 'wind', 'wind', 'snowflake',
		'cloud', 'cloud_moon', 'cloud_sun', 'cloud_moon', 'cloud_sun', 'moon', 'sun',
		'moon', 'sun', 'hail', 'sun', 'lightning', 'lightning', 'lightning', 'rain',
		'snowflake', 'snowflake', 'snowflake', 'cloud', 'rain', 'snow', 'lightning'
	];
	
	var weatherDiv = $('#weather_sdalmacija'),
		scroller = $('#scroller_sdalmacija'),
		location = $('p.location_sdalmacija');
	

	
	/* Configuration */
	
	sdalmacija(); 
	
	function sdalmacija() {
	    var lat = 44.11520;
	    var lon = 15.23666;

	    // We are passing the R gflag for reverse geocoding (coordinates to place name)
	    var geoAPI = 'http://where.yahooapis.com/geocode?location='+lat+','+lon+'&flags=J&gflags=R&appid='+APPID;
	    
	    // Forming the query for Yahoo's weather forecasting API with YQL
	    // http://developer.yahoo.com/weather/

	    var wsql = 'select * from weather.forecast where woeid=WID and u="'+DEG+'"',
	        weatherYQL = 'http://query.yahooapis.com/v1/public/yql?q='+encodeURIComponent(wsql)+'&format=json&callback=?',
	        code, city, results, woeid;
	    
	    if (window.console && window.console.info){
	    	console.info("Coordinates: %f %f", lat, lon);
	    }
	    
	    // Issue a cross-domain AJAX request (CORS) to the GEO service.
	    // Not supported in Opera and IE.
	    $.getJSON(geoAPI, function(r){
	       
	        if(r.ResultSet.Found == 1){
	        	
	            results = r.ResultSet.Results;
	            city = results[0].city;
	            code = results[0].statecode || results[0].countrycode;
	    
	            // This is the city identifier for the weather API
	            woeid = results[0].woeid;
	
	            // Make a weather API request:
	            $.getJSON(weatherYQL.replace('WID',woeid), function(r){
	            	
	                if(r.query && r.query.count == 1){
	                	
	                	// Create the weather items in the #scroller UL
	                	
	                    var item = r.query.results.channel.item.condition;
	                    
	                    if(!item){
	                    	showError("We can't find weather information about your city!");
	                    	if (window.console && window.console.info){
						    	console.info("%s, %s; woeid: %d", city, code, woeid);
						    }
						    
						    return false;
	                    }
	                    
	                    addWeather_sdalmacija(item.code, "LIVE", ' <b>'+item.temp+'°'+DEG+'</b>');
	                    
	                    // Add the location to the page
	                    location.html();
	                    weatherDiv.addClass('loaded');
	                   	               
	                }
	            });
	    
	        }
	        
	    }).error(function(){
	    	showError("Your browser does not support CORS requests!");
	    });
	   
	}
	
	function addWeather_sdalmacija(code, day, condition){
		
	    var markup = '<li>'+
	    	'<img src="images/weather/'+ weatherIconMap[code] +'.png" />'+
	    	' <p class="day">'+ day +'</p> <p class="cond">'+ condition +
	    	'</p></li>';
	    
	    scroller.append(markup);
	}

});








/* --------------------- WEATHER JDALMACIJA --------------------- */

$(function weather_jdalmacija(){
	
	/* Configuration */
	
	var APPID = 'CwCOKf32';		// Your Yahoo APP id
	var DEG = 'c';		// c for celsius, f for fahrenheit
	
	// Mapping the weather codes returned by Yahoo's API
	// to the correct icons in the img/icons folder
	
	var weatherIconMap = [
		'storm', 'storm', 'storm', 'lightning', 'lightning', 'snow', 'hail', 'hail',
		'drizzle', 'drizzle', 'rain', 'rain', 'rain', 'snow', 'snow', 'snow', 'snow',
		'hail', 'hail', 'fog', 'fog', 'fog', 'fog', 'wind', 'wind', 'snowflake',
		'cloud', 'cloud_moon', 'cloud_sun', 'cloud_moon', 'cloud_sun', 'moon', 'sun',
		'moon', 'sun', 'hail', 'sun', 'lightning', 'lightning', 'lightning', 'rain',
		'snowflake', 'snowflake', 'snowflake', 'cloud', 'rain', 'snow', 'lightning'
	];
	
	var weatherDiv = $('#weather_jdalmacija'),
		scroller = $('#scroller_jdalmacija'),
		location = $('p.location_jdalmacija');
	

	
	/* Configuration */
	
	jdalmacija(); 
	
	function jdalmacija() {
	    var lat = 43.29607;
	    var lon = 17.01731;

	    // We are passing the R gflag for reverse geocoding (coordinates to place name)
	    var geoAPI = 'http://where.yahooapis.com/geocode?location='+lat+','+lon+'&flags=J&gflags=R&appid='+APPID;
	    
	    // Forming the query for Yahoo's weather forecasting API with YQL
	    // http://developer.yahoo.com/weather/

	    var wsql = 'select * from weather.forecast where woeid=WID and u="'+DEG+'"',
	        weatherYQL = 'http://query.yahooapis.com/v1/public/yql?q='+encodeURIComponent(wsql)+'&format=json&callback=?',
	        code, city, results, woeid;
	    
	    if (window.console && window.console.info){
	    	console.info("Coordinates: %f %f", lat, lon);
	    }
	    
	    // Issue a cross-domain AJAX request (CORS) to the GEO service.
	    // Not supported in Opera and IE.
	    $.getJSON(geoAPI, function(r){
	       
	        if(r.ResultSet.Found == 1){
	        	
	            results = r.ResultSet.Results;
	            city = results[0].city;
	            code = results[0].statecode || results[0].countrycode;
	    
	            // This is the city identifier for the weather API
	            woeid = results[0].woeid;
	
	            // Make a weather API request:
	            $.getJSON(weatherYQL.replace('WID',woeid), function(r){
	            	
	                if(r.query && r.query.count == 1){
	                	
	                	// Create the weather items in the #scroller UL
	                	
	                    var item = r.query.results.channel.item.condition;
	                    
	                    if(!item){
	                    	showError("We can't find weather information about your city!");
	                    	if (window.console && window.console.info){
						    	console.info("%s, %s; woeid: %d", city, code, woeid);
						    }
						    
						    return false;
	                    }
	                    
	                    addWeather_jdalmacija(item.code, "LIVE", ' <b>'+item.temp+'°'+DEG+'</b>');
	                    
	                    // Add the location to the page
	                    location.html();
	                    weatherDiv.addClass('loaded');
	                   	               
	                }
	            });
	    
	        }
	        
	    }).error(function(){
	    	showError("Your browser does not support CORS requests!");
	    });
	   
	}
	
	function addWeather_jdalmacija(code, day, condition){
		
	    var markup = '<li>'+
	    	'<img src="images/weather/'+ weatherIconMap[code] +'.png" />'+
	    	' <p class="day">'+ day +'</p> <p class="cond">'+ condition +
	    	'</p></li>';
	    
	    scroller.append(markup);
	}

});




