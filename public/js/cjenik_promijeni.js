var t;
promjeniTekst();

$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD')) 
             || d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});

function promjeniTekst(){
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Polja označena zvijezdicom su obavezna.");
            }
        };
        elements[i].onchange = function(e) {
            e.target.setCustomValidity("");
        };
    }

	var selements = document.getElementsByTagName("SELECT");	
    for (var i = 0; i < selements.length; i++) {
        selements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Polja označena zvijezdicom su obavezna.");
            }
        };
        selements[i].onchange = function(e) {
            e.target.setCustomValidity("");
        };
    }
}


function dodaj_cijenu(){
	var br_redova = window.localStorage.getItem("br_redova");
	$('#cjenik').animate({height:'+=29px'}, 0);
	var cjenik;

	if(document.getElementById('cijena_noc').checked){
		cjenik = 'cijena_noc';
	}else if(document.getElementById('cijena_osoba').checked){
		cjenik = 'cijena_osoba';
	}else{
		cjenik = 'cijena_napredno';
	}
	
    var values = {br_redova:br_redova};
    $.get('../../config/cjenik.php?job=1&cjenik='+cjenik, values,function(data) {
        $('#tablica').append(data);
		var element = parent.document.getElementById(window.name);		
		parent.resizeIframe(element);
		promjeniTekst();
    });

	window.localStorage.setItem("br_redova", (+br_redova+1));
	document.getElementById('br_redova').value = (+br_redova+1);
}

function cjenik(cijena){

	var cjenik = cijena.id;
	var br_redova = window.localStorage.getItem("br_redova");
	var datum = [];
	var min = [];
	var price = [];
	var cjenik_vrsta = window.localStorage.getItem("cjenik_vrsta");
	
	if(document.getElementById('cijena_napredno').checked){
		var osoba = +document.getElementById('lezaj').value + +document.getElementById('pomocni').value;
	}

	for (var i=1; i<=(+br_redova*2); i++){
		datum.push(document.getElementById('datum' + i).value);
	}

	for (var i=1; i<=(+br_redova); i++){
		min.push(document.getElementById('min' + i).value);
	}

	if(cjenik_vrsta == 'cijena_napredno'){
		for (var i=1; i<=(+br_redova*3); i=i+3){
			price.push(document.getElementById('price' + i).value);
		}	
	}else{
		for (var i=1; i<=(+br_redova); i++){
			price.push(document.getElementById('price' + i).value);
		}
	}	


	$('#tablica').load('../../config/cjenik.php?job=2&cjenik='+cjenik+'&br_redova='+br_redova+'&osoba='+osoba, function(){
		for (var i=1; i<=(+br_redova*2); i++){
			document.getElementById('datum' + i).value = datum[i-1];
		}
		
		for (var i=1; i<=(+br_redova); i++){
			document.getElementById('min' + i).value = min[i-1];
		}

		if(document.getElementById('cijena_napredno').checked){	
			for (var i=1, j=1; i<=(+br_redova*3); i=i+3, j++){
				document.getElementById('price' + i).value = price[j-1];
			}
		}else{
			for (var i=1; i<=(+br_redova); i++){
				document.getElementById('price' + i).value = price[i-1];
			}	
		}

		promjeniTekst();
	});
	
	window.localStorage.setItem("cjenik_vrsta",cjenik);	
}

function cjenik_delay(){
	if ( t ){
		clearTimeout( t );
		t = setTimeout( cjenik_vrste, 500 );
	}else{
		t = setTimeout( cjenik_vrste, 500 );
	}
}

function cjenik_vrste(){
	var osoba = +document.getElementById('lezaj').value + +document.getElementById('pomocni').value;
	var br_redova = window.localStorage.getItem("br_redova");
	var datum = [];
	var min = [];
	var price = [];
	var cjenik;
	var cjenik_vrsta = window.localStorage.getItem("cjenik_vrsta");
	
	if(document.getElementById('cijena_noc').checked || osoba <3){
		cjenik = 'cijena_noc';
	}else if(document.getElementById('cijena_osoba').checked){
		cjenik = 'cijena_osoba';
	}else{
		cjenik = 'cijena_napredno';
	}
	
	for (var i=1; i<=(+br_redova*2); i++){
		datum.push(document.getElementById('datum' + i).value);
	}

	for (var i=1; i<=(+br_redova); i++){
		min.push(document.getElementById('min' + i).value);
	}
	
	if(cjenik_vrsta == 'cijena_napredno'){
		for (var i=1; i<=(+br_redova*3); i=i+3){
			price.push(document.getElementById('price' + i).value);
		}	
	}else{
		for (var i=1; i<=(+br_redova); i++){
			price.push(document.getElementById('price' + i).value);
		}
	}


	$('#cjenik_vrste').load('../../config/cjenik.php?job=3&cjenik='+cjenik+'&osoba='+osoba);
	$('#tablica').load('../../config/cjenik.php?job=2&cjenik='+cjenik+'&br_redova='+br_redova+'&osoba='+osoba, function(){
		for (var i=1; i<=(+br_redova*2); i++){
			document.getElementById('datum' + i).value = datum[i-1];
		}
		
		for (var i=1; i<=(+br_redova); i++){
			document.getElementById('min' + i).value = min[i-1];
		}
		
		if(document.getElementById('cijena_napredno').checked){	
			for (var i=1, j=1; i<=(+br_redova*3); i=i+3, j++){
				document.getElementById('price' + i).value = price[j-1];
			}
		}else{
			for (var i=1; i<=(+br_redova); i++){
				document.getElementById('price' + i).value = price[i-1];
			}	
		}
		
		promjeniTekst();		
	});

	window.localStorage.setItem("cjenik_vrsta",cjenik);
}